tableextension 60054 "SMK SalesCommentLie" extends "Sales Comment Line"
{
    fields
    {
        field(60000; "SMK Bank Ref. 1"; Text[20])
        {
            DataClassification = ToBeClassified;
        }
        field(60001; "SMK Bank Ref. 2"; Text[20])
        {
            DataClassification = ToBeClassified;
        }
        field(60002; "SMK Amount"; Decimal)
        {
            DataClassification = ToBeClassified;
        }
        field(60003; "SMK Pay Date"; Date)
        {
            DataClassification = ToBeClassified;
        }
        field(60004; "SMK Payment Type"; Code[20])
        {
            DataClassification = ToBeClassified;
            TableRelation = "Dimension Value".Code where("Dimension Code" = const('PAYMENTTYPE'));
        }
    }
}