page 60015 "SMK Front Bank Deposit List"
{
    PageType = List;
    UsageCategory = Lists;
    SourceTable = "Purchase Header";
    SourceTableView = where("Document Type" = const("SMK Front Bank Deposit"));
    DeleteAllowed = false;
    CardPageId = "SMK Front Bank Deposit Card";
    layout
    {
        area(Content)
        {
            repeater("Content1")
            {
                field("No."; Rec."No.")
                {
                    ApplicationArea = All;
                }
                field("Document Date"; Rec."Document Date")
                {
                    ApplicationArea = All;
                }
                field("SMK Cash Amount"; Rec."SMK Cash Amount")
                {
                    Caption = 'Cash Amountssssxxxzzz';
                    ApplicationArea = All;
                }
                field("SMK Summary Cash Amount in Day"; Rec."SMK Summary Cash Amount in Day")
                {
                    Caption = 'Summary Cash Amount in Daygggggg';
                    ApplicationArea = All;
                }

            }
        }
    }
}