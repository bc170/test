page 60012 "SMK Front Receipt Insurrance"
{
    AutoSplitKey = true;
    Caption = 'Insurrace Lines';
    DelayedInsert = true;
    LinksAllowed = false;
    MultipleNewLines = true;
    PageType = ListPart;
    SourceTable = "Sales Line";
    SourceTableView = WHERE("Document Type" = FILTER("SMK Front Receipt"));
    Editable = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    layout
    {
        area(Content)
        {
            repeater("Content1")
            {
                field("SMK Ref. Insurrance No."; Rec."SMK Policy No.")
                {
                    Caption = 'Insurrance No.';
                    ApplicationArea = All;
                }
                field("SMK Ref. Cust. Ledg. Entry No."; Rec."SMK Ref. Cust. Ledg. Entry No.")
                {
                    Caption = 'Ref. Entry No.';
                    ApplicationArea = All;
                }
                field(Amount; Rec.Amount)
                {
                    ApplicationArea = All;
                }
            }
        }
    }
}