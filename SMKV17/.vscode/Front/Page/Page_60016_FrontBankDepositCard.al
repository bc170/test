page 60016 "SMK Front Bank Deposit Card"
{
    PageType = Card;
    SourceTable = "Purchase Header";
    SourceTableView = where("Document Type" = const("SMK Front Bank Deposit"));
    DeleteAllowed = false;
    layout
    {
        area(Content)
        {
            group("General")
            {
                field("No."; Rec."No.")
                {
                    ApplicationArea = All;
                }
                field("Document Date"; Rec."Document Date")
                {
                    ApplicationArea = All;
                }
                field("SMK Cash Amount"; Rec."SMK Cash Amount")
                {
                    Caption = 'Cash Amount';
                    ApplicationArea = All;
                }
                field("SMK Summary Cash Amount in Day"; Rec."SMK Summary Cash Amount in Day")
                {
                    Caption = 'Summary Cash Amount in Day';
                    ApplicationArea = All;
                }
                field("SMK Deposit Date"; Rec."SMK Deposit Date")
                {
                    Caption = 'Deposit Date';
                    ApplicationArea = All;
                }
                field("SMK Bank Ref. 1"; Rec."SMK Bank Ref. 1")
                {
                    Caption = 'Bank Reference 1';
                    ApplicationArea = All;
                }
                field("SMK Bank Ref. 2"; Rec."SMK Bank Ref. 2")
                {
                    Caption = 'Bank Reference 2';
                    ApplicationArea = All;
                }
            }
        }
    }
}