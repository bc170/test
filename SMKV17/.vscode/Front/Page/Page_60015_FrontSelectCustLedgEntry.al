page 60014 "SMK FrontSelectCustLedgEntry"
{
    PageType = List;
    UsageCategory = Lists;
    SourceTable = "Cust. Ledger Entry";
    DeleteAllowed = false;
    SourceTableTemporary = true;
    Editable = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    Caption = 'Front Select Cust. Ledg. Entry';
    layout
    {
        area(Content)
        {
            repeater("Content1")
            {
                field("Document No."; Rec."Document No.")
                {
                    ApplicationArea = All;
                }
                field("SMK Ref. Insurrance No."; Rec."SMK Ref. Policy No.")
                {
                    ApplicationArea = All;
                }
                field("Customer No."; Rec."Customer No.")
                {
                    ApplicationArea = All;
                }
                field("Customer Name"; Rec."Customer Name")
                {
                    ApplicationArea = All;
                }
                field("Remaining Amount"; Rec."Remaining Amount")
                {
                    ApplicationArea = All;
                }
            }
        }
    }
}