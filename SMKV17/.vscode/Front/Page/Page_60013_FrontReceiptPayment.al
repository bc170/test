page 60013 "SMK Front Receipt Payment"
{
    AutoSplitKey = true;
    Caption = 'Payment Lines';
    DelayedInsert = true;
    LinksAllowed = false;
    MultipleNewLines = true;
    PageType = ListPart;
    SourceTable = "Sales Comment Line";
    SourceTableView = WHERE("Document Type" = FILTER("SMK Front Receipt"));
    Editable = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    layout
    {
        area(Content)
        {
            repeater("Content1")
            {
                field("SMK Pay Date"; Rec."SMK Pay Date")
                {
                    Caption = 'Payment Date';
                    ApplicationArea = All;
                }
                field("SMK Bank Ref. 1"; Rec."SMK Bank Ref. 1")
                {
                    Caption = 'Bank Reference 1';
                    ApplicationArea = All;
                }
                field("SMK Bank Ref. 2"; Rec."SMK Bank Ref. 2")
                {
                    Caption = 'Bank Reference 2';
                    ApplicationArea = All;
                }
                field("SMK Amount"; Rec."SMK Amount")
                {
                    Caption = 'Amount';
                    ApplicationArea = All;
                }
            }
        }
    }
}