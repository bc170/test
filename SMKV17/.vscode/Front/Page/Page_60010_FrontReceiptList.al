page 60010 "SMK Front Receipt List"
{
    PageType = List;
    UsageCategory = Lists;
    SourceTable = "Sales Header";
    SourceTableView = where("Document Type" = const("SMK Front Receipt"));
    DeleteAllowed = false;
    CardPageId = "SMK Front Receipt Card";
    layout
    {
        area(Content)
        {
            repeater("Content1")
            {
                field("No."; Rec."No.")
                {
                    ApplicationArea = All;
                }
                field("Document Date"; Rec."Document Date")
                {
                    ApplicationArea = All;
                }

                field("Sell-to Customer No."; Rec."Sell-to Customer No.")
                {
                    Caption = 'Customer No.';
                    ApplicationArea = All;
                }
                field("Sell-to Customer Name"; Rec."Sell-to Customer Name")
                {
                    Caption = 'Customer Name';
                    ApplicationArea = All;
                }
            }
        }
    }
}