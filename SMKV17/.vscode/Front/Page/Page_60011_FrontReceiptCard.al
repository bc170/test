page 60011 "SMK Front Receipt Card"
{
    SourceTable = "Sales Header";
    SourceTableView = where("Document Type" = const("SMK Front Receipt"));
    PageType = Card;
    layout
    {
        area(Content)
        {
            group("Scan")
            {
                field(ScanValue; ScanValue)
                {
                    Caption = 'Scan';
                    ApplicationArea = All;
                    trigger OnValidate()
                    var
                        CustLedgEntry: Record "Cust. Ledger Entry";
                        TempCustLedgEntry: Record "Cust. Ledger Entry" temporary;
                        FrontSelectCustLedgEntry: Page "SMK FrontSelectCustLedgEntry";
                    begin
                        Clear(TempCustLedgEntry);
                        CustLedgEntry.reset;
                        CustLedgEntry.SetRange("Document No.", ScanValue);
                        CustLedgEntry.SetFilter("Remaining Amount", '<>0');
                        if CustLedgEntry.FindSet() then begin
                            repeat
                                TempCustLedgEntry.reset;
                                TempCustLedgEntry.SetRange("Entry No.", CustLedgEntry."Entry No.");
                                if TempCustLedgEntry.IsEmpty then begin
                                    TempCustLedgEntry.TransferFields(CustLedgEntry);
                                    TempCustLedgEntry.Insert();
                                end;
                            until CustLedgEntry.Next() = 0;
                        end;
                        CustLedgEntry.reset;
                        CustLedgEntry.SetRange("SMK Ref. Policy No.", ScanValue);
                        CustLedgEntry.SetFilter("Remaining Amount", '<>0');
                        if CustLedgEntry.FindSet() then begin
                            repeat
                                TempCustLedgEntry.reset;
                                TempCustLedgEntry.SetRange("Entry No.", CustLedgEntry."Entry No.");
                                if TempCustLedgEntry.IsEmpty then begin
                                    TempCustLedgEntry.TransferFields(CustLedgEntry);
                                    TempCustLedgEntry.Insert();
                                end;
                            until CustLedgEntry.Next() = 0;
                        end;
                        Clear(FrontSelectCustLedgEntry);
                        FrontSelectCustLedgEntry.LookupMode(true);
                        FrontSelectCustLedgEntry.SetRecord(TempCustLedgEntry);
                        FrontSelectCustLedgEntry.RunModal();

                        ScanValue := '';

                    end;
                }
            }
            group("Detail")
            {
                field("Sell-to Customer No."; Rec."Sell-to Customer No.")
                {
                    Caption = 'Customer No.';
                    ApplicationArea = All;
                }
                field("Sell-to Customer Name"; Rec."Sell-to Customer Name")
                {
                    Caption = 'Customer Name';
                }
            }

            part(InsurranceLine; "SMK Front Receipt Insurrance")
            {
                ApplicationArea = Basic, Suite;
                Editable = false;
                SubPageLink = "Document No." = FIELD("No.");
                UpdatePropagation = Both;
            }
            part(PaymentLine; "SMK Front Receipt Payment")
            {
                ApplicationArea = Basic, Suite;
                Editable = false;
                SubPageLink = "No." = FIELD("No.");
                UpdatePropagation = Both;
            }
        }
    }

    actions
    {
        area(Processing)
        {
            action("Print Receipt")
            {
                Image = Print;
                Promoted = true;
                PromotedIsBig = true;
                PromotedOnly = true;
                PromotedCategory = Report;
                trigger OnAction()
                begin
                    Message('wait...');
                end;
            }
        }
    }
    /// <summary> 
    /// Function For Scan to get data from cust. ledg. entry Add by CT 17/08/2020
    /// </summary>
    local procedure "SMK ScanValue"()
    begin

    end;

    var
        ScanValue: text[20];
}