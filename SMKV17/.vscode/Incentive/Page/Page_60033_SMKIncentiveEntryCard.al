page 60033 "SMK Incentive Entry Card"
{
    PageType = Card;
    SourceTable = "SMK Incentive Entry";
    RefreshOnActivate = true;
    Caption = 'Incentive Entry Card';

    layout
    {
        area(Content)
        {
            group("General")
            {
                Caption = 'General';
                field("Document No."; Rec."Document No.")
                {
                    ApplicationArea = All;
                }
                field("Calculate Date"; Rec."Calculate Date")
                {
                    ApplicationArea = All;
                }
                field(Amount; Rec.Amount)
                {
                    ApplicationArea = All;
                    Editable = False;
                }
                field("Approve Status"; Rec."Approve Status")
                {
                    ApplicationArea = All;
                }
                field("Approve Date"; Rec."Approve Date")
                {
                    ApplicationArea = All;
                }
            }
            part(IncentiveEntrySubForm; "SMK Incentive Entry Detail Sub")
            {
                Caption = 'Detail';
                SubPageLink = "Document No." = Field("Document No.");
            }
        }
    }

    actions
    {
        area(Processing)
        {
            action(SendApprove)
            {
                ApplicationArea = All;
                Caption = 'Send Approve';
                Promoted = true;
                Image = SendApprovalRequest;
                trigger OnAction()
                begin

                end;
            }
            action(CancelApprove)
            {
                ApplicationArea = All;
                Caption = 'Cancel Approve';
                Promoted = true;
                Image = CancelApprovalRequest;
                trigger OnAction()
                begin

                end;
            }
        }
    }
}