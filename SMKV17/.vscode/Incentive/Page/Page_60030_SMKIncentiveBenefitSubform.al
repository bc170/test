page 60030 "SMK Incentive Benefit Subform"
{
    PageType = ListPart;
    SourceTable = "SMK Incentive Benefit";
    Caption = 'Benefit';
    DelayedInsert = true;
    MultipleNewLines = true;
    AutoSplitKey = true;

    layout
    {
        area(Content)
        {
            repeater(Control1)
            {
                field("Policy Code"; Rec."Policy Code")
                {
                    Editable = False;
                    Visible = False;
                }
                field("Entry No."; Rec."Entry No.")
                {
                    ApplicationArea = All;
                    Caption = 'No.';
                    Visible = false;
                }
                field("Department Code"; Rec."Department Code")
                {
                    ApplicationArea = All;
                }
                field("Department Name"; Rec."Department Name")
                {
                    ApplicationArea = All;
                }
                field("Position Code"; Rec."Position Code")
                {
                    ApplicationArea = All;
                }
                field("Position Name"; Rec."Position Name")
                {
                    ApplicationArea = All;
                }
                field("Benefit %"; Rec."Benefit %")
                {
                    ApplicationArea = All;
                }
                field("benefit Amount"; Rec."benefit Amount")
                {
                    ApplicationArea = All;
                }
            }
        }
    }

}