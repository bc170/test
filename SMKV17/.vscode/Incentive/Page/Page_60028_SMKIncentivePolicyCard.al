page 60028 "SMK Incentive Policy Card"
{
    PageType = Card;
    //ApplicationArea = All;
    UsageCategory = Lists;
    RefreshOnActivate = true;
    SourceTable = "SMK Incentive Policy";
    Caption = 'Incentive Policy Card';

    layout
    {
        area(Content)
        {
            group("General")
            {
                Caption = 'General';
                field(Code; Rec.Code)
                {
                    ApplicationArea = All;
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = All;
                }
                field("Branch Code"; Rec."Branch Code")
                {
                    ApplicationArea = All;
                }
                field("Branch Name"; Rec."Branch Name")
                {
                    ApplicationArea = All;
                }
                field("Starting Date"; Rec."Starting Date")
                {
                    ApplicationArea = All;
                }
                field("Ending Date"; Rec."Ending Date")
                {
                    ApplicationArea = All;
                }
                field("Approve Status"; Rec."Approve Status")
                {
                    ApplicationArea = All;
                }
                field("Approve Date"; Rec."Approve Date")
                {
                    ApplicationArea = All;
                }
                field(Enable; Rec.Enable)
                {
                    ApplicationArea = All;
                }
                field("Disable Date"; Rec."Disable Date")
                {
                    ApplicationArea = All;
                }

            }
            part(IncentiveFactoreSubForm; "SMK Incentive Factor Subform")
            {
                Caption = 'Factor';
                SubPageLink = "Policy Code" = field(Code);
            }
            part(IncentiveBenifitSubForm; "SMK Incentive Benefit Subform")
            {
                Caption = 'Benefit';
                SubPageLink = "Policy Code" = field(Code);
            }

        }
    }

    actions
    {

        area(Processing)
        {
            action(SendApprove)
            {
                ApplicationArea = All;
                Caption = 'Send Approve';
                Promoted = true;
                Image = SendApprovalRequest;
                trigger OnAction()
                begin

                end;
            }
            action(CancelApprove)
            {
                ApplicationArea = All;
                Caption = 'Cancel Approve';
                Promoted = true;
                Image = CancelApprovalRequest;
                trigger OnAction()
                begin

                end;
            }
            action(ActionEnable)
            {
                ApplicationArea = All;
                Caption = 'Enable';
                Promoted = true;
                trigger OnAction()
                begin

                end;
            }
            action(ActionDisable)
            {
                ApplicationArea = All;
                Caption = 'Disable';
                Promoted = true;
                trigger OnAction()
                begin

                end;
            }
        }
    }
}