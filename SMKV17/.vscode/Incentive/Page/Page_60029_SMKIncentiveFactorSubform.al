page 60029 "SMK Incentive Factor Subform"
{
    PageType = ListPart;
    SourceTable = "SMK Incentive Factor";
    Caption = 'Factor';

    layout
    {
        area(Content)
        {
            repeater(Control1)
            {
                field("Policy Code"; Rec."Policy Code")
                {
                    Editable = False;
                    Visible = False;
                }
                field("Field No."; Rec."Field No.")
                {
                    ApplicationArea = All;
                }
                field("Field Name"; Rec."Field Name")
                {
                    ApplicationArea = All;
                }
                field("Filter Expression"; Rec."Filter Expression")
                {
                    ApplicationArea = All;
                }
            }
        }
    }
}