page 60034 "SMK Incentive Entry List"
{
    PageType = List;
    ApplicationArea = All;
    UsageCategory = Lists;
    SourceTable = "SMK Incentive Entry";
    Caption = 'Incentive Entry';
    Editable = false;
    CardPageId = "SMK Incentive Entry Card";
    RefreshOnActivate = true;

    layout
    {
        area(Content)
        {
            repeater(Control1)
            {
                field("Document No."; Rec."Document No.")
                {
                    ApplicationArea = All;
                }
                field("Calculate Date"; Rec."Calculate Date")
                {
                    ApplicationArea = All;
                }
                field(Amount; Rec.Amount)
                {
                    ApplicationArea = All;
                }
                field("Approve Status"; Rec."Approve Status")
                {
                    ApplicationArea = All;
                }
                field("Approve Date"; Rec."Approve Date")
                {
                    ApplicationArea = All;
                }
            }
        }
    }

    actions
    {
        area(Processing)
        {
            action(SendApprove)
            {
                ApplicationArea = All;
                Caption = 'Send Approve';
                Image = SendApprovalRequest;
                trigger OnAction()
                begin
                    Message('Send Approve');
                end;
            }
            action(CancelApprove)
            {
                ApplicationArea = All;
                Caption = 'Cancel Approve';
                Image = CancelApprovalRequest;
                trigger OnAction()
                begin
                    Message('Cancel Approve');
                end;
            }
        }
    }
}