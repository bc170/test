page 60031 "SMK Incentive Policy List"
{
    PageType = List;
    ApplicationArea = All;
    UsageCategory = Lists;
    SourceTable = "SMK Incentive Policy";
    Caption = 'Incentive Policy';
    Editable = false;
    CardPageId = "SMK Incentive Policy Card";
    RefreshOnActivate = true;

    layout
    {
        area(Content)
        {
            repeater(Control1)
            {
                field(Code; Rec.Code)
                {
                    ApplicationArea = All;
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = All;
                }
                field("Branch Code"; Rec."Branch Code")
                {
                    ApplicationArea = All;
                }
                field("Branch Name"; Rec."Branch Name")
                {
                    ApplicationArea = All;
                }
                field("Starting Date"; Rec."Starting Date")
                {
                    ApplicationArea = All;
                }
                field("Ending Date"; Rec."Ending Date")
                {
                    ApplicationArea = All;
                }
                field("Approve Status"; Rec."Approve Status")
                {
                    ApplicationArea = All;
                }
                field("Approve Date"; Rec."Approve Date")
                {
                    ApplicationArea = All;
                }
                field(Enable; Rec.Enable)
                {
                    ApplicationArea = All;
                }
                field("Disable Date"; Rec."Disable Date")
                {
                    ApplicationArea = All;
                }

            }
        }
    }

    actions
    {

        area(Processing)
        {
            action(SendApprove)
            {
                ApplicationArea = All;
                Caption = 'Send Approve';
                Image = SendApprovalRequest;
                trigger OnAction()
                begin

                end;
            }
            action(CancelApprove)
            {
                ApplicationArea = All;
                Caption = 'Cancel Approve';
                Image = CancelApprovalRequest;
                trigger OnAction()
                begin

                end;
            }
        }
    }
}