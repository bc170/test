page 60032 "SMK Incentive Entry Detail Sub"
{
    PageType = ListPart;
    SourceTable = "SMK Incentive Entry Detail";
    Caption = 'Detail';


    layout
    {
        area(Content)
        {
            repeater(Control1)
            {

                field("Document No."; Rec."Document No.")
                {

                    Visible = false;
                }
                field("Entry No."; Rec."Entry No.")
                {
                    ApplicationArea = All;
                    Visible = false;
                }
                field("Order No."; Rec."Order No.")
                {
                    ApplicationArea = All;
                }
                field("Policy No."; Rec."Policy No.")
                {
                    ApplicationArea = All;
                }
                field("Branch Code"; Rec."Branch Code")
                {
                    ApplicationArea = All;
                }
                field("Department Code"; Rec."Department Code")
                {
                    ApplicationArea = All;
                }
                field("Position Code"; Rec."Position Code")
                {
                    ApplicationArea = All;
                }
                field("Benefit %"; Rec."Benefit %")
                {
                    ApplicationArea = All;
                }
                field("Benefit Amount"; Rec."Benefit Amount")
                {
                    ApplicationArea = All;
                }
                field("Benefit Total"; Rec."Benefit Total")
                {
                    ApplicationArea = All;
                }
            }
        }
    }
}