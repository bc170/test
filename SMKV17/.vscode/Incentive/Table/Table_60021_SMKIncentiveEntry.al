table 60021 "SMK Incentive Entry"
{
    DataClassification = CustomerContent;

    fields
    {
        field(1; "Document No."; Code[20])
        {
            DataClassification = CustomerContent;
        }
        field(2; "Calculate Date"; DateTime)
        {
            DataClassification = CustomerContent;
        }
        field(3; "Amount"; Decimal)
        {
            FieldClass = FlowField;
            CalcFormula = sum("SMK Incentive Entry Detail"."Benefit Total" where("Document No." = field("Document No.")));
        }
        field(4; "Approve Status"; option)
        {
            OptionMembers = " ","Open","Pending Approve","Released";
            DataClassification = CustomerContent;
        }
        field(5; "Approve Date"; Date)
        {
            DataClassification = CustomerContent;
        }
    }

    keys
    {
        key(Key1; "Document No.")
        {
            Clustered = true;
        }
    }
}