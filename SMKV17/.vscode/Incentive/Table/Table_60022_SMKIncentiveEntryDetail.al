table 60022 "SMK Incentive Entry Detail"
{
    DataClassification = CustomerContent;

    fields
    {
        field(1; "Document No."; COde[20])
        {
            DataClassification = CustomerContent;
            TableRelation = "SMK Incentive Entry"."Document No.";
        }
        field(2; "Entry No."; Integer)
        {
            DataClassification = CustomerContent;
        }
        field(3; "Calculate Date"; DateTime)
        {
            DataClassification = CustomerContent;
        }
        field(4; "Incentive Code"; Code[20])
        {
            DataClassification = CustomerContent;
        }

        field(5; "Order No."; Code[50])
        {
            DataClassification = CustomerContent;
        }
        field(6; "Policy No."; Code[50])
        {
            DataClassification = CustomerContent;
        }
        field(7; "Branch Code"; Code[20])
        {
            DataClassification = CustomerContent;
            TableRelation = "Dimension Value".Code where("Dimension Code" = const('BRANCH'));
        }
        field(8; "Department Code"; Code[20])
        {
            DataClassification = CustomerContent;
            TableRelation = "Dimension Value".Code where("Dimension Code" = const('DEPARTMENT'));
        }
        field(9; "Position Code"; Code[20])
        {
            DataClassification = CustomerContent;
            TableRelation = "Dimension Value".Code where("Dimension Code" = const('JOBPOSITION'));
        }
        field(10; "Benefit Amount"; Decimal)
        {
            DataClassification = CustomerContent;
        }
        field(11; "Benefit %"; Decimal)
        {
            DataClassification = CustomerContent;
        }
        field(12; "Benefit Total"; Decimal)
        {
            DataClassification = CustomerContent;
        }
    }

    keys
    {
        key(Key1; "Document No.", "Entry No.")
        {
            Clustered = true;
        }
    }

    trigger OnInsert()
    begin
        "Entry No." := GetLastEntryNo();
    end;

    procedure GetLastEntryNo(): Integer
    var
        RecIncentiveEntryDetail: Record "SMK Incentive Entry Detail";
        LastEntryNo: Integer;
    begin
        LastEntryNo := 0;
        RecIncentiveEntryDetail.Reset();
        if RecIncentiveEntryDetail.FindLast() then
            LastEntryNo := RecIncentiveEntryDetail."Entry No.";

        LastEntryNo := LastEntryNo + 1;
        exit(LastEntryNo);
    end;

}