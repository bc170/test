table 60019 "SMK Incentive Factor"
{
    DataClassification = CustomerContent;

    fields
    {
        field(1; "Policy Code"; Code[20])
        {
            DataClassification = CustomerContent;
            TableRelation = "SMK Incentive Policy".Code;
        }
        field(2; "Field No."; Integer)
        {
            DataClassification = CustomerContent;
            TableRelation = Field."No." where(TableNo = const(70000));
        }
        field(3; "Field Name"; Text[100])
        {
            FieldClass = FlowField;
            CalcFormula = lookup(Field."Field Caption" where(TableNo = const(70000), "No." = field("Field No.")));
        }
        field(4; "Filter Expression"; Text[250])
        {
            DataClassification = CustomerContent;
        }
    }

    keys
    {
        key(Key1; "Policy Code", "Field No.")
        {
            Clustered = true;
        }
    }
}