table 60020 "SMK Incentive Benefit"
{
    DataClassification = CustomerContent;

    fields
    {
        field(1; "Policy Code"; Code[20])
        {
            DataClassification = CustomerContent;
            TableRelation = "SMK Incentive Policy".Code;

        }
        field(2; "Entry No."; Integer)
        {
            DataClassification = CustomerContent;
        }
        field(3; "Department Code"; Code[20])
        {
            DataClassification = CustomerContent;
            TableRelation = "Dimension Value".Code where("Dimension Code" = const('DEPARTMENT'));
        }
        field(4; "Department Name"; Text[50])
        {
            FieldClass = FlowField;
            CalcFormula = lookup("Dimension Value".Name where("Dimension Code" = const('DEPARTMENT'), "Code" = field("Department Code")));
        }

        field(5; "Position Code"; Code[20])
        {
            DataClassification = CustomerContent;
            TableRelation = "Dimension Value".Code where("Dimension Code" = const('JOBPOSITION'));
        }
        field(6; "Position Name"; Text[50])
        {
            FieldClass = FlowField;
            CalcFormula = lookup("Dimension Value".Name where("Dimension Code" = const('JOBPOSITION'), "Code" = field("Position Code")));
        }
        field(7; "Benefit %"; Decimal)
        {
            DataClassification = CustomerContent;
        }
        field(8; "benefit Amount"; Decimal)
        {
            DataClassification = CustomerContent;
        }
        field(9; "Currency Code"; Code[20])
        {
            DataClassification = CustomerContent;
            TableRelation = Currency.Code;
        }
    }

    keys
    {
        key(Key1; "Policy Code", "Entry No.")
        {
            Clustered = true;
        }
    }
    trigger OnInsert()
    begin
        "Entry No." := GetLastEntryNo();
    end;

    procedure GetLastEntryNo(): Integer
    var
        RecIncentiveBenefit: Record "SMK Incentive Benefit";
        LastEntryNo: Integer;
    begin
        LastEntryNo := 0;
        RecIncentiveBenefit.Reset();
        if RecIncentiveBenefit.FindLast() then
            LastEntryNo := RecIncentiveBenefit."Entry No.";

        LastEntryNo := LastEntryNo + 1;
        exit(LastEntryNo);
    end;

}