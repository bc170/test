table 60018 "SMK Incentive Policy"
{
    DataClassification = CustomerContent;

    fields
    {

        field(1; "Code"; Code[20])
        {
            DataClassification = CustomerContent;

        }
        field(2; "Description"; Text[100])
        {
            DataClassification = CustomerContent;
        }
        field(3; "Branch Code"; Code[20])
        {
            DataClassification = CustomerContent;
            TableRelation = "Dimension Value"."Code" where("Dimension Code" = const('BRANCH'));
        }
        field(4; "Branch Name"; Text[50])
        {
            FieldClass = FlowField;
            CalcFormula = lookup("Dimension Value"."Name" where("Dimension Code" = const('BRANCH'), "Code" = field("Branch Code")));
        }
        field(5; "Starting Date"; Date)
        {
            DataClassification = CustomerContent;
        }
        field(6; "Ending Date"; Date)
        {
            DataClassification = CustomerContent;
        }
        field(7; "Approve Status"; option)
        {
            OptionMembers = " ","Open","Pending Approve","Released";
            DataClassification = CustomerContent;
        }
        field(8; "Approve Date"; Date)
        {
            DataClassification = CustomerContent;
        }
        field(9; "Enable"; Boolean)
        {
            DataClassification = CustomerContent;
        }
        field(10; "Disable Date"; Blob)
        {
            DataClassification = CustomerContent;
        }
    }

    keys
    {
        key(Key1; Code)
        {
            Clustered = true;
        }
    }
}