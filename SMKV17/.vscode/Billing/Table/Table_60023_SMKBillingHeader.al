table 60023 "SMK Billing Header"
{
    DataClassification = CustomerContent;

    fields
    {
        field(1; "Document No."; Code[20])
        {
            DataClassification = CustomerContent;
        }
        field(2; "Document Date"; Date)
        {
            DataClassification = CustomerContent;
        }
        field(3; "Customer No."; Code[20])
        {
            DataClassification = CustomerContent;
            TableRelation = Customer."No.";
        }
        field(4; "Customer Name"; Text[50])
        {
            FieldClass = FlowField;
            CalcFormula = Lookup(Customer.Name Where("No." = field("Customer No.")));
        }
        field(5; "Amount"; Decimal)
        {
            FieldClass = FlowField;
            CalcFormula = Sum("SMK Billing Line".Amount Where("Document No." = field("Document No.")));
        }
        field(6; "Discount Amount"; Decimal)
        {
            FieldClass = FlowField;
            CalcFormula = Sum("SMK Billing Line".Discount Where("Document No." = field("Document No.")));
        }
        field(7; "Promotion Disc. Amt."; Decimal)
        {
            FieldClass = FlowField;
            CalcFormula = Sum("SMK Billing Promo. Disc.Line".Amount Where("Document No." = field("Document No.")));
        }
    }

    keys
    {
        key(Key1; "Document No.")
        {
            Clustered = true;
        }
    }
}