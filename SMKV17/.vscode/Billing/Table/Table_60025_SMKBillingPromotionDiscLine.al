table 60025 "SMK Billing Promo. Disc.Line"
{
    DataClassification = CustomerContent;

    fields
    {
        field(1; "Document No."; Code[20])
        {
            DataClassification = CustomerContent;
            TableRelation = "SMK Billing Header"."Document No.";
        }
        field(2; "Line No."; Integer)
        {
            DataClassification = SystemMetadata;
        }
        field(3; "Entry No."; Integer)
        {
            DataClassification = CustomerContent;
        }
        field(4; "Promotion Code"; Code[20])
        {
            DataClassification = CustomerContent;
        }
        field(5; "Calculate Date"; Date)
        {
            DataClassification = CustomerContent;
        }
        field(6; "Amount"; Decimal)
        {
            DataClassification = CustomerContent;
        }
    }

    keys
    {
        key(Key1; "Document No.", "Line No.")
        {
            Clustered = true;
        }
    }

    trigger OnInsert()
    begin
        "Entry No." := GetLastEntryNo();
    end;

    procedure GetLastEntryNo(): Integer
    var
        RecBillingPromoDiscLine: Record "SMK Billing Promo. Disc.Line";
        LastEntryNo: Integer;
    begin
        LastEntryNo := 0;
        RecBillingPromoDiscLine.Reset();
        if RecBillingPromoDiscLine.FindLast() then
            LastEntryNo := RecBillingPromoDiscLine."Entry No.";

        LastEntryNo := LastEntryNo + 1;
        exit(LastEntryNo);
    end;

}