page 60038 "SMK Billing List"
{
    PageType = List;
    ApplicationArea = All;
    UsageCategory = Lists;
    SourceTable = "SMK Billing Header";
    Caption = 'Billing List';
    RefreshOnActivate = true;
    CardPageId = "SMK Billing Card";

    layout
    {
        area(Content)
        {
            repeater(Control1)
            {
                field("Document No."; Rec."Document No.")
                {
                    ApplicationArea = All;
                }
                field("Document Date"; Rec."Document Date")
                {
                    ApplicationArea = All;
                }
                field("Customer No."; Rec."Customer No.")
                {
                    ApplicationArea = All;
                }
                field("Customer Name"; Rec."Customer Name")
                {
                    ApplicationArea = All;
                }
                field(Amount; Rec.Amount)
                {
                    ApplicationArea = All;
                }
            }
        }
    }

    actions
    {
        area(Processing)
        {
            action(GenBilling)
            {
                ApplicationArea = All;
                Caption = 'Generate Billing';
                Image = GetEntries;

                trigger OnAction()
                begin
                    Message('Generate..');
                end;
            }
            action(ExportTextFile)
            {
                ApplicationArea = All;
                Caption = 'Export Text File';
                Image = ExportFile;

                trigger OnAction()
                begin
                    Message('Export Text File..');
                end;
            }
        }
    }
}