page 60035 "SMK Billing Card"
{
    PageType = Card;
    RefreshOnActivate = true;
    SourceTable = "SMK Billing Header";
    Caption = 'Billing Card TestLLL';

    layout
    {
        area(Content)
        {
            group(GroupName)
            {
                Caption = 'General';
                field("Document No."; Rec."Document No.")
                {
                    ApplicationArea = All;
                }
                field("Document Date"; Rec."Document Date")
                {
                    ApplicationArea = All;
                }
                field("Customer No."; Rec."Customer No.")
                {
                    ApplicationArea = All;
                }
                field("Customer Name"; Rec."Customer Name")
                {
                    ApplicationArea = All;
                }
                field(Amount; Rec.Amount)
                {
                    ApplicationArea = All;
                    Editable = False;
                }
                field("Discount Amount"; Rec."Discount Amount")
                {
                    ApplicationArea = All;
                    Editable = False;
                }
                field("Promotion Disc. Amt."; Rec."Promotion Disc. Amt.")
                {
                    ApplicationArea = All;
                    Editable = False;
                }
            }
            part(BillingLineSubForm; "SMK Billing Line Subform")
            {
                Caption = 'Billing Line';
                SubPageLink = "Document No." = field("Document No.");
            }
            part(BillinkPromoDiscSubForm; "SMK Billing Promo. Disc. Sub")
            {
                Caption = 'Billing Promotion Discount';
                SubPageLink = "Document No." = field("Document No.");
            }
        }
    }

    actions
    {
        area(Processing)
        {
            action(ExportTextFile)
            {
                ApplicationArea = All;
                Caption = 'Export Text File';
                Image = ExportFile;

                trigger OnAction()
                begin

                end;
            }
            action(GetCustLedger)
            {
                ApplicationArea = All;
                Caption = 'Get Cust. Ledger';
                Image = CustomerLedger;

                trigger OnAction()
                begin

                end;
            }
            action(PrintBilling)
            {
                ApplicationArea = All;
                Caption = 'พิมพ์ใบวางบิล';
                Image = Print;

                trigger OnAction()
                begin
                    Message('Print Billing..');
                end;
            }
        }
    }
}