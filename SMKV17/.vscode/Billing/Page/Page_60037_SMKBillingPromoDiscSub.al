page 60037 "SMK Billing Promo. Disc. Sub"
{
    PageType = ListPart;
    SourceTable = "SMK Billing Promo. Disc.Line";
    Caption = 'Billing Promotion Discount';
    DelayedInsert = true;
    MultipleNewLines = true;
    AutoSplitKey = true;

    layout
    {
        area(Content)
        {
            repeater(Control1)
            {
                field("Document No."; Rec."Document No.")
                {
                    Visible = False;
                }
                field("Line No."; Rec."Line No.")
                {
                    Editable = false;
                    Visible = false;
                }
                field("Entry No."; Rec."Entry No.")
                {
                    ApplicationArea = All;
                    Visible = false;
                }
                field("Promotion Code"; Rec."Promotion Code")
                {
                    ApplicationArea = All;
                }
                field("Calculate Date"; Rec."Calculate Date")
                {
                    ApplicationArea = All;
                }
                field(Amount; Rec.Amount)
                {
                    ApplicationArea = All;
                }
            }
        }
    }
}