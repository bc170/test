table 60006 "SMK Promo. Disc. Factor"
{
    fields
    {
        field(1; "SMK Promotion Code"; Code[20])
        {
            DataClassification = ToBeClassified;
            TableRelation = "SMK Promo. Disc. Setup"."SMK Promotion Code";
        }
        field(2; "SMK Factor Field"; Integer)
        {
            DataClassification = ToBeClassified;
            TableRelation = Field."No." where(TableNo = const(60002));
        }
        field(3; "SMK Factor Field Name"; Text[250])
        {
            FieldClass = FlowField;
            CalcFormula = lookup(Field."Field Caption" where(TableNo = const(60002), "No." = field("SMK Factor Field")));
        }
        field(4; "SMK Filter Expression"; Text[250])
        {
            DataClassification = ToBeClassified;
        }
    }
    keys
    {
        key(PrimaryKey; "SMK Promotion Code", "SMK Factor Field")
        {
            Clustered = true;
        }
    }
}