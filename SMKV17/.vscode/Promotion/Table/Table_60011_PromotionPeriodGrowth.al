table 60011 "SMK Promo. Period. Growth"
{

    fields
    {
        field(1; "SMK Promotion Code"; Code[20])
        {
            DataClassification = ToBeClassified;
        }
        field(2; "SMK Line No."; Integer)
        {
            DataClassification = ToBeClassified;
        }
        field(3; "SMK Growthup % Start"; Decimal)
        {
            DataClassification = ToBeClassified;
        }
        field(4; "SMK Growthup % End"; Decimal)
        {
            DataClassification = ToBeClassified;
        }
        field(5; "SMK Benefit Amount"; Decimal)
        {
            DataClassification = ToBeClassified;
        }
        field(6; "SMK Benefit %"; Decimal)
        {
            DataClassification = ToBeClassified;
        }
        field(7; "SMK Benefit Amount by Policy"; Decimal)
        {
            DataClassification = ToBeClassified;
        }
        field(8; "SMK Benefit % by Policy"; Decimal)
        {
            DataClassification = ToBeClassified;
        }
    }
    keys
    {
        key(PrimaryKey; "SMK Promotion Code", "SMK Line No.")
        {
            Clustered = true;
        }
    }
}