table 60008 "SMK Promo. Period. Setup"
{

    fields
    {
        field(1; "SMK Promotion Code"; Code[20])
        {
            DataClassification = ToBeClassified;
        }
        field(2; "SMK Description"; Text[100])
        {
            DataClassification = ToBeClassified;
        }
        field(3; "SMK Starting Date"; Date)
        {
            DataClassification = ToBeClassified;
        }
        field(4; "SMK Ending Date"; Date)
        {
            DataClassification = ToBeClassified;
        }
        field(5; "SMK Disable"; Boolean)
        {
            DataClassification = ToBeClassified;
        }
        field(6; "SMK Create Date"; DateTime)
        {
            DataClassification = SystemMetadata;
        }
        field(7; "SMK Create User"; Code[50])
        {
            DataClassification = SystemMetadata;
        }
        field(8; "SMK Approve Status"; Enum "Purchase Document Status")
        {
            DataClassification = SystemMetadata;
        }
        field(9; "SMK Promotion Type"; Enum "SMK Period Promotion Type")
        {
            DataClassification = ToBeClassified;
        }
        field(10; "SMK Before Start Date"; Date)
        {
            DataClassification = ToBeClassified;
        }
        field(11; "SMK Before End Date"; Date)
        {
            DataClassification = ToBeClassified;
        }
        field(12; "SMK After Start Date"; Date)
        {
            DataClassification = ToBeClassified;
        }
        field(13; "SMK After End Date"; Date)
        {
            DataClassification = ToBeClassified;
        }
        field(14; "SMK Target Date Field No."; Integer)
        {
            DataClassification = ToBeClassified;
            TableRelation = Field."No." where(TableNo = const(60002), Type = filter(Date | DateTime));
        }
        field(15; "SMK Target Date Caption"; Text[100])
        {
            FieldClass = FlowField;
            CalcFormula = lookup(Field."Field Caption" where(TableNo = const(60002), "No." = field("SMK Target Date Field No.")));
        }
        field(16; "SMK Discount %"; Decimal)
        {
            DataClassification = ToBeClassified;
        }
        field(17; "SMK Discount Amount"; Decimal)
        {
            DataClassification = ToBeClassified;
        }
    }
    keys
    {
        key(PrimaryKey; "SMK Promotion Code")
        {
            Clustered = true;
        }
    }
    trigger OnInsert()
    var
        LoginInfor: Record "BW LogInformation";
    begin
        if LoginInfor.GET(SessionId(), ServiceInstanceId()) then begin
            "SMK Create User" := LoginInfor."BW USER Name";
            "SMK Create Date" := CurrentDateTime;
        end;
    end;
}