table 60010 "SMK Promo. Period. Benefit"
{

    fields
    {
        field(1; "SMK Promotion Code"; Code[20])
        {
            DataClassification = ToBeClassified;
        }
        field(2; "SMK Line No."; Integer)
        {
            DataClassification = ToBeClassified;
        }
        field(3; "SMK Benefit Type"; Option)
        {
            DataClassification = ToBeClassified;
            OptionMembers = " ","By Policy","By Amount";
        }
        field(4; "SMK Policy Qty. Start"; Integer)
        {
            DataClassification = ToBeClassified;
        }
        field(5; "SMK Policy Qty. End"; Integer)
        {
            DataClassification = ToBeClassified;
        }
        field(6; "SMK Policy Amount Start"; Decimal)
        {
            DataClassification = ToBeClassified;
        }
        field(7; "SMK Policy Amount End"; Decimal)
        {
            DataClassification = ToBeClassified;
        }
        field(8; "SMK Benefit %"; Decimal)
        {
            DataClassification = ToBeClassified;
        }
        field(9; "SMK Benefit Amount"; Decimal)
        {
            DataClassification = ToBeClassified;
        }
    }
    keys
    {
        key(PrimaryKey; "SMK Promotion Code", "SMK Line No.")
        {
            Clustered = true;
        }
    }
}