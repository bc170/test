table 60005 "SMK Promo. Disc. Setup"
{

    fields
    {
        field(1; "SMK Promotion Code"; Code[20])
        {
            DataClassification = ToBeClassified;
        }
        field(2; "SMK Description"; Text[100])
        {
            DataClassification = ToBeClassified;
        }
        field(3; "SMK Starting Date"; Date)
        {
            DataClassification = ToBeClassified;
        }
        field(4; "SMK Ending Date"; Date)
        {
            DataClassification = ToBeClassified;
        }
        field(5; "SMK Disable"; Boolean)
        {
            DataClassification = ToBeClassified;
        }
        field(6; "SMK Create Date"; DateTime)
        {
            DataClassification = SystemMetadata;
        }
        field(7; "SMK Create User"; Code[50])
        {
            DataClassification = SystemMetadata;
        }
        field(8; "SMK Approve Status"; Enum "Purchase Document Status")
        {
            DataClassification = SystemMetadata;
        }
        field(9; "SMK Discount %"; Decimal)
        {
            DataClassification = ToBeClassified;
            MinValue = 0;
        }
        field(10; "SMK Discount Amount"; Decimal)
        {
            DataClassification = ToBeClassified;
            MinValue = 0;
        }
        field(11; "SMK ID"; Guid)
        {
            DataClassification = SystemMetadata;
            Editable = false;
        }

    }

    keys
    {
        key(PrimaryKey; "SMK Promotion Code")
        {
            Clustered = true;
        }
    }

    trigger OnInsert()
    var
        LoginInfor: Record "BW LogInformation";
    begin
        if LoginInfor.GET(SessionId(), ServiceInstanceId()) then begin
            "SMK Create User" := LoginInfor."BW USER Name";
            "SMK Create Date" := CurrentDateTime;
        end;
    end;
}