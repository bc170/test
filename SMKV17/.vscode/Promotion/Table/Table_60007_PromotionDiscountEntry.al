table 60007 "SMK Promo. Disc. Entry"
{

    fields
    {
        field(1; "SMK Entry No."; Integer)
        {
            DataClassification = SystemMetadata;
        }
        field(2; "SMK Promotion Code"; Code[20])
        {
            DataClassification = SystemMetadata;
        }
        field(3; "SMK Policy Code"; Text[50])
        {
            DataClassification = SystemMetadata;
        }
        field(4; "SMK Discount Amount"; Decimal)
        {
            DataClassification = ToBeClassified;
        }
        field(5; "SMK Used By"; Text[250])
        {
            DataClassification = ToBeClassified;
        }
        field(6; "SMK Used By Record"; RecordId)
        {
            DataClassification = ToBeClassified;
        }
    }
    keys
    {
        key(PrimaryKey; "SMK Entry No.")
        {
            Clustered = true;
        }
    }
    /// <summary> 
    /// Description for getLastEntryNo.
    /// </summary>
    /// <returns>Return variable "Integer".</returns>
    procedure "SMK getLastEntryNo"(): Integer
    var
        PromoDiscEntry: Record "SMK Promo. Disc. Entry";
    begin
        PromoDiscEntry.reset;
        if PromoDiscEntry.FindLast() then
            exit(PromoDiscEntry."SMK Entry No.")
        else
            exit(0);

    end;
}