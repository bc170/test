page 60006 "SMK Disc. Promo. Card"
{
    SourceTable = "SMK Promo. Disc. Setup";
    PageType = Card;
    Caption = 'Discount Promotion Setup';
    layout
    {
        area(Content)
        {
            group("General")
            {
                field("SMK Promotion Code"; Rec."SMK Promotion Code")
                {
                    Caption = 'รหัสโปรโมชั่น';
                    ApplicationArea = All;
                }
                field("SMK Description"; Rec."SMK Description")
                {
                    Caption = 'รายละเอียด';
                    ApplicationArea = All;
                }
                field("SMK Starting Date"; Rec."SMK Starting Date")
                {
                    Caption = 'วันที่เริ่ม';
                    ApplicationArea = All;
                }
                field("SMK Ending Date"; Rec."SMK Ending Date")
                {
                    Caption = 'วันที่สิ้นสุด';
                    ApplicationArea = All;
                }
                field("SMK Disable"; Rec."SMK Disable")
                {
                    Caption = 'ยกเลิก';
                    ApplicationArea = All;
                }
                field("SMK Discount %"; Rec."SMK Discount %")
                {
                    Caption = 'ส่วนลด %';
                    ApplicationArea = All;
                }
                field("SMK Discount Amount"; Rec."SMK Discount Amount")
                {
                    Caption = 'ส่วนลด(จำนวนเงิน)';
                    ApplicationArea = All;
                }
                field("SMK Create Date"; Rec."SMK Create Date")
                {
                    Caption = 'วันที่สร้าง';
                    ApplicationArea = All;
                }
                field("SMK Create User"; Rec."SMK Create User")
                {
                    Caption = 'ผู้สร้าง';
                    ApplicationArea = All;
                }
                field("SMK Approve Status"; Rec."SMK Approve Status")
                {
                    Caption = 'สถานะการอนุมัติ';
                    ApplicationArea = All;
                }
            }
            part(BWKPromoFactor; "SMK Disc. Promo. Factor")
            {
                Caption = 'Factor';
                SubPageLink = "SMK Promotion Code" = field("SMK Promotion Code");
                ApplicationArea = All;
            }

        }
    }
    actions
    {
        area(Processing)
        {
            action("Test Calc. Preview")
            {
                ApplicationArea = All;
                Caption = 'Test Calc. Preview';
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                Image = TestReport;
                trigger OnAction()
                var
                    LPromoDiscEntryReview: Page "SMK Promotion Entry Review";
                begin
                    LPromoDiscEntryReview."SMK SetPromotionCode"(Rec);
                    LPromoDiscEntryReview.RunModal();
                end;
            }
            action("Send Apporve")
            {
                ApplicationArea = All;
                Caption = 'ส่งขออนุมัติ';
                Promoted = true;
                Enabled = Rec."SMK Approve Status" = Rec."SMK Approve Status"::Open;
                PromotedCategory = Process;
                PromotedIsBig = true;
                Image = SendApprovalRequest;
                trigger OnAction()
                begin
                    SMKApproveMgt.OnSendDiscPromotionforApproval(Rec);
                end;
            }
            action("Cancel Apporve Request")
            {
                ApplicationArea = All;
                Caption = 'ยกเลิกการขออนุมัติ';
                Promoted = true;
                Enabled = Rec."SMK Approve Status" = Rec."SMK Approve Status"::"Pending Approval";
                PromotedCategory = Process;
                PromotedIsBig = true;
                Image = CancelApprovalRequest;
                trigger OnAction()
                begin
                    SMKApproveMgt.OnCancelDiscPromotionforApproval(Rec);
                end;
            }
            action("Reopen")
            {
                ApplicationArea = All;
                Caption = 'ยกเลิกสถานะการอนุมัติ';
                Promoted = true;
                Enabled = Rec."SMK Approve Status" = Rec."SMK Approve Status"::Released;
                PromotedCategory = Process;
                PromotedIsBig = true;
                Image = ReOpen;
                trigger OnAction()
                begin
                    SMKApproveMgt.OnCancelDiscPromotionforApproval(Rec);
                end;
            }
        }
    }
    var
        SMKApproveMgt: Codeunit "SMK Init Workflow";
}