page 60020 "SMK Period Promo. Bene. Policy"
{
    SourceTable = "SMK Promo. Period. Benefit";
    PageType = ListPart;
    AutoSplitKey = true;
    DelayedInsert = true;
    SourceTableView = where("SMK Benefit Type" = const("By Policy"));
    layout
    {
        area(Content)
        {
            repeater(Content1)
            {
                field(SMKPromotionCode; Rec."SMK Promotion Code")
                {
                    Visible = false;
                    ApplicationArea = All;
                }
                field(SMKLineNo; Rec."SMK Line No.")
                {
                    Visible = false;
                    ApplicationArea = All;
                }
                field(SMKQtyStart; Rec."SMK Policy Qty. Start")
                {
                    ApplicationArea = All;
                    Caption = 'จำนวนใบเริ่มต้น';
                }
                field(SMKQtyEnd; Rec."SMK Policy Qty. End")
                {
                    ApplicationArea = All;
                    Caption = 'จำนวนใบสิ้นสุด';
                }
                field(SMKBenefitAmount; Rec."SMK Benefit Amount")
                {
                    ApplicationArea = All;
                    Caption = 'ผลตอบแทนเป็นจำนวนเงิน';
                }
                field(SMKBenefitPercent; Rec."SMK Benefit %")
                {
                    ApplicationArea = All;
                    Caption = 'ผลตอบแทนเป็น %';
                }
            }
        }
    }
}