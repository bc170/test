page 60007 "SMK Disc. Promo. Factor"
{
    SourceTable = "SMK Promo. Disc. Factor";
    PageType = ListPart;
    Caption = 'Discount Promotion Factor';
    layout
    {
        area(Content)
        {
            repeater("Content1")
            {
                field("SMK Factor Field"; Rec."SMK Factor Field")
                {
                    ApplicationArea = All;
                    trigger OnDrillDown()
                    var
                        PageFieldSelection: page "SMK Field Selection";
                        RecField: Record Field;
                    begin
                        RecField.reset;
                        RecField.SetRange(TableNo, 60002);
                        Clear(PageFieldSelection);
                        PageFieldSelection.LookupMode(true);
                        PageFieldSelection.SetRecord(RecField);
                        PageFieldSelection.SetTableView(RecField);
                        if PageFieldSelection.RunModal() = Action::LookupOK then begin
                            PageFieldSelection.GetRecord(RecField);
                            Rec."SMK Factor Field" := RecField."No.";
                        end;
                    end;

                    trigger OnAssistEdit()
                    var
                        PageFieldSelection: page "SMK Field Selection";
                        RecField: Record Field;
                    begin
                        RecField.reset;
                        RecField.SetRange(TableNo, 60002);
                        Clear(PageFieldSelection);
                        PageFieldSelection.LookupMode(true);
                        PageFieldSelection.SetRecord(RecField);
                        PageFieldSelection.SetTableView(RecField);
                        if PageFieldSelection.RunModal() = Action::LookupOK then begin
                            PageFieldSelection.GetRecord(RecField);
                            Rec."SMK Factor Field" := RecField."No.";
                        end;
                    end;
                }
                field("BW Factor Field Name"; Rec."SMK Factor Field Name")
                {
                    ApplicationArea = All;
                    Editable = false;
                }
                field("BW Filter Expression"; Rec."SMK Filter Expression")
                {
                    ApplicationArea = All;
                }
            }
        }
    }
}