page 60021 "SMK Period Promo. Bene. Amount"
{
    SourceTable = "SMK Promo. Period. Benefit";
    PageType = ListPart;
    AutoSplitKey = true;
    DelayedInsert = true;
    SourceTableView = where("SMK Benefit Type" = const("By Amount"));
    layout
    {
        area(Content)
        {
            repeater(Content1)
            {
                field(SMKPromotionCode; Rec."SMK Promotion Code")
                {
                    Visible = false;
                    ApplicationArea = All;
                }
                field(SMKLineNo; Rec."SMK Line No.")
                {
                    Visible = false;
                    ApplicationArea = All;
                }
                field(SMKAmountStart; Rec."SMK Policy Amount Start")
                {
                    ApplicationArea = All;
                    Caption = 'ยอดขายตั้งต้น';
                }
                field(SMKAmountEnd; Rec."SMK Policy Amount End")
                {
                    ApplicationArea = All;
                    Caption = 'ยอดขายสิ้นสุด';
                }
                field(SMKBenefitAmount; Rec."SMK Benefit Amount")
                {
                    ApplicationArea = All;
                    Caption = 'ผลตอบแทนเป็นจำนวนเงิน';
                }
                field(SMKBenefitPercent; Rec."SMK Benefit %")
                {
                    ApplicationArea = All;
                    Caption = 'ผลตอบแทนเป็น %';
                }
            }
        }
    }
}