page 60022 "SMK Period Promo. Bene. Growth"
{
    SourceTable = "SMK Promo. Period. Growth";
    PageType = ListPart;
    AutoSplitKey = true;
    DelayedInsert = true;
    layout
    {
        area(Content)
        {
            repeater(content1)
            {
                field(SMKPromotionCode; Rec."SMK Promotion Code")
                {
                    Visible = false;
                    ApplicationArea = All;
                }
                field(SMKLineNo; Rec."SMK Line No.")
                {
                    Visible = false;
                    ApplicationArea = All;
                }
                field(SMKGrowthupStart; Rec."SMK Growthup % Start")
                {
                    ApplicationArea = All;
                    Caption = '% อัตราเติบโตเริ่มต้น';
                }
                field("SMKGrowthupEnd"; Rec."SMK Growthup % End")
                {
                    ApplicationArea = All;
                    Caption = '% อัตราเติบโตสิ้นสุด';
                }
                field(SMKBenefitAmount; Rec."SMK Benefit Amount")
                {
                    ApplicationArea = All;
                    Caption = 'ผลตอบแทนเป็นจำนวนเงิน';
                }
                field(SMKBenefitAmountByPolicy; Rec."SMK Benefit Amount by Policy")
                {
                    ApplicationArea = All;
                    Caption = 'ผลตอบแทนเป็นจำนวนเงินตามจำนวนกธ.';
                }
                field(SMKBenefitPercent; Rec."SMK Benefit %")
                {
                    ApplicationArea = All;
                    Caption = 'ผลตอบแทนเป็น %';
                }
            }
        }
    }
}