page 60008 "SMK Promotion Entry Review"
{
    SourceTable = "SMK Promo. Disc. Entry";
    PageType = List;
    SourceTableTemporary = true;
    Editable = false;
    layout
    {
        area(Content)
        {
            repeater("Content1")
            {
                field("SMK Promotion Code"; Rec."SMK Promotion Code")
                {
                    ApplicationArea = All;
                }
                field("SMK Policy Code"; Rec."SMK Policy Code")
                {
                    ApplicationArea = All;
                }
                field("SMK Discount Amount"; Rec."SMK Discount Amount")
                {
                    ApplicationArea = All;
                }
            }
        }
    }

    trigger OnOpenPage()
    begin
        GSMKFunction."SMK RunPromotionDiscountPreviewPage"(GPromoDiscSetup, Rec);
    end;

    /// <summary> 
    /// Description for BWK SetPromotionCode.
    /// </summary>
    /// <param name="PPromoDiscSetup">Parameter of type Record "SMK Promo. Disc. Setup".</param>
    procedure "SMK SetPromotionCode"(PPromoDiscSetup: Record "SMK Promo. Disc. Setup")
    begin
        GPromoDiscSetup := PPromoDiscSetup;
    end;

    var
        GPromoDiscCode: Code[20];
        GPromoDiscSetup: Record "SMK Promo. Disc. Setup";
        GSMKFunction: Codeunit "SMK Function";
}