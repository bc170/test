page 60023 "SMK Period Promo. Bene. Ratio"
{
    SourceTable = "SMK Promo. Period. Ratio";
    PageType = ListPart;
    AutoSplitKey = true;
    DelayedInsert = true;
    layout
    {
        area(Content)
        {
            repeater(content1)
            {
                field(SMKPromotionCode; Rec."SMK Promotion Code")
                {
                    Visible = false;
                    ApplicationArea = All;
                }
                field(SMKLineNo; Rec."SMK Line No.")
                {
                    Visible = false;
                    ApplicationArea = All;
                }
                field(SMKLossStart; Rec."SMK Loss % Start")
                {
                    ApplicationArea = All;
                    Caption = '% Loss เริ่มต้น';
                }
                field(SMKLossEnd; Rec."SMK Loss % End")
                {
                    ApplicationArea = All;
                    Caption = '% Loss สิ้นสุด';
                }

                field(SMKBenefitAmount; Rec."SMK Benefit Amount")
                {
                    ApplicationArea = All;
                    Caption = 'ผลตอบแทนเป็นจำนวนเงิน';
                }
                field(SMKBenefitAmountByPolicy; Rec."SMK Benefit Amount by Policy")
                {
                    ApplicationArea = All;
                    Caption = 'ผลตอบแทนเป็นจำนวนเงินตามจำนวนใบกธ.';
                }
                field(SMKBenefitPercent; Rec."SMK Benefit %")
                {
                    ApplicationArea = All;
                    Caption = 'ผลตอบแทนเป็น %';
                }
            }
        }
    }
}