page 60018 "SMK Period Promo. Card"
{
    SourceTable = "SMK Promo. Period. Setup";
    PageType = Card;
    layout
    {
        area(Content)
        {
            group(General)
            {
                Caption = 'ทั่วไป';
                field(SMKPromotionCode; Rec."SMK Promotion Code")
                {
                    ApplicationArea = All;
                    Caption = 'รหัสโปรโมชั่น';
                }
                field(SMKDescription; Rec."SMK Description")
                {
                    ApplicationArea = All;
                    Caption = 'รายละเอียด';
                }
                field(SMKStartingDate; Rec."SMK Starting Date")
                {
                    ApplicationArea = All;
                    Caption = 'วันที่สร้าง';
                }
                field(SMKEndingDate; Rec."SMK Ending Date")
                {
                    ApplicationArea = All;
                    Caption = 'วันที่สิ้นสุดด';
                }
                field(SMKDisable; Rec."SMK Disable")
                {
                    ApplicationArea = All;
                    Caption = 'ยกเลิก';
                }
                field(SMKPromotionType; Rec."SMK Promotion Type")
                {
                    ApplicationArea = All;
                    Caption = 'รูปแบบโปรโมชั่น';
                }
                field(SMKApproveStatus; Rec."SMK Approve Status")
                {
                    ApplicationArea = All;
                    Caption = 'สถานะการอนุมัติ';
                }
                field(SMKCreateDate; Rec."SMK Create Date")
                {
                    ApplicationArea = All;
                    Caption = 'วันที่สร้าง';
                }
                field(SMKCreateUser; Rec."SMK Create User")
                {
                    ApplicationArea = All;
                    Caption = 'ผู้สร้าง';
                }
            }
            part(SMKPromoFactor; "SMK Period Promo. Factor")
            {
                Caption = 'Factor';
                SubPageLink = "SMK Promotion Code" = field("SMK Promotion Code");
                ApplicationArea = All;
            }
            group(GroupBenefitPolicy)
            {
                Caption = 'ผลตอบแทนตามจำนวนใบกรมธรรม์';
                Visible = ((Rec."SMK Promotion Type" = Rec."SMK Promotion Type"::"SMK Benefit by Policy") or (Rec."SMK Promotion Type" = Rec."SMK Promotion Type"::"SMK Benefit by Policy&Amount"));
                part(PartSMKBenefitPoliy;
                "SMK Period Promo. Bene. Policy")
                {
                    Caption = 'ผลตอบแทนตามจำนวนใบกรมธรรม์';
                    SubPageLink = "SMK Promotion Code" = field("SMK Promotion Code");
                    ApplicationArea = All;
                }
            }
            group(GroupBenefitAmount)
            {
                Caption = 'ผลตอบแทนตามยอดขาย';

                Visible = ((Rec."SMK Promotion Type" = Rec."SMK Promotion Type"::"SMK Benefit by Amount") or (Rec."SMK Promotion Type" = Rec."SMK Promotion Type"::"SMK Benefit by Policy&Amount"));
                part(PartSMKBenefitAmount; "SMK Period Promo. Bene. Amount")
                {
                    Caption = 'ผลตอบแทนตามยอดขาย';
                    SubPageLink = "SMK Promotion Code" = field("SMK Promotion Code");
                    ApplicationArea = All;
                }
            }
            group(BenefitGrowthup)
            {
                Caption = 'ผลตอบแทนตามอัตราตามการเติบโต';
                Visible = (Rec."SMK Promotion Type" = Rec."SMK Promotion Type"::"SMK Benefit by Growthup");
                field(SMKTargetDateFieldNo; Rec."SMK Target Date Field No.")
                {
                    ApplicationArea = All;
                    Caption = 'รหัส Field วันที่เป้าหมาย';
                    trigger OnDrillDown()
                    var
                        PageFieldSelection: page "SMK Field Selection";
                        RecField: Record Field;
                    begin
                        RecField.reset;
                        RecField.SetRange(TableNo, 60002);
                        RecField.SetFilter(Type, '%1|%2', RecField.Type::Date, RecField.Type::DateTime);
                        Clear(PageFieldSelection);
                        PageFieldSelection.LookupMode(true);
                        PageFieldSelection.SetRecord(RecField);
                        PageFieldSelection.SetTableView(RecField);
                        if PageFieldSelection.RunModal() = Action::LookupOK then begin
                            PageFieldSelection.GetRecord(RecField);
                            Rec."SMK Target Date Field No." := RecField."No.";
                            Rec.Modify();
                        end;
                    end;

                    trigger OnAssistEdit()
                    var
                        PageFieldSelection: page "SMK Field Selection";
                        RecField: Record Field;
                    begin
                        RecField.reset;
                        RecField.SetRange(TableNo, 60002);
                        RecField.SetFilter(Type, '%1|%2', RecField.Type::Date, RecField.Type::DateTime);
                        Clear(PageFieldSelection);
                        PageFieldSelection.LookupMode(true);
                        PageFieldSelection.SetRecord(RecField);
                        PageFieldSelection.SetTableView(RecField);
                        if PageFieldSelection.RunModal() = Action::LookupOK then begin
                            PageFieldSelection.GetRecord(RecField);
                            Rec."SMK Target Date Field No." := RecField."No.";
                            Rec.Modify();
                        end;
                    end;
                }
                field(SMKTargetDateFieldCaption; Rec."SMK Target Date Caption")
                {
                    ApplicationArea = All;
                    Editable = false;
                    Caption = 'Caption Field วันที่เป้าหมาย';
                }
                field(SMKBeforeStartDate; Rec."SMK Before Start Date")
                {
                    ApplicationArea = All;
                    Caption = 'วันที่เริ่ม Period ตั้งต้น';
                }
                field(SMKBeforeEndDate; Rec."SMK Before End Date")
                {
                    ApplicationArea = All;
                    Caption = 'วันที่สิ้นสุด Period ตั้งต้น';
                }
                field(SMKAfterStartDate; Rec."SMK After Start Date")
                {
                    ApplicationArea = All;
                    Caption = 'วันที่เริ่ม Period เป้าหมาย';
                }
                field(SMKAfterEndDate; Rec."SMK After End Date")
                {
                    ApplicationArea = All;
                    Caption = 'วันที่สิ้นสุด Period เป้าหมาย';
                }
                part(PartSMKBenefitGrowthup;
                "SMK Period Promo. Bene. Growth")
                {
                    Caption = 'ผลตอบแทนตามอัตราตามการเติบโต';
                    SubPageLink = "SMK Promotion Code" = field("SMK Promotion Code");
                    ApplicationArea = All;
                }
            }
            group(BenefitLossRatio)
            {
                Caption = 'ผลตอบแทนตามผลต่างสินไหม';
                Visible = (Rec."SMK Promotion Type" = Rec."SMK Promotion Type"::"SMK Benefit by Loss Ratio");
                field(SMKTargetDateFieldNo2; Rec."SMK Target Date Field No.")
                {
                    ApplicationArea = All;
                    Caption = 'รหัส Field วันที่เป้าหมาย';
                    trigger OnDrillDown()
                    var
                        PageFieldSelection: page "SMK Field Selection";
                        RecField: Record Field;
                    begin
                        RecField.reset;
                        RecField.SetRange(TableNo, 60002);
                        RecField.SetFilter(Type, '%1|%2', RecField.Type::Date, RecField.Type::DateTime);
                        Clear(PageFieldSelection);
                        PageFieldSelection.LookupMode(true);
                        PageFieldSelection.SetRecord(RecField);
                        PageFieldSelection.SetTableView(RecField);
                        if PageFieldSelection.RunModal() = Action::LookupOK then begin
                            PageFieldSelection.GetRecord(RecField);
                            Rec."SMK Target Date Field No." := RecField."No.";
                            Rec.Modify();
                        end;
                    end;

                    trigger OnAssistEdit()
                    var
                        PageFieldSelection: page "SMK Field Selection";
                        RecField: Record Field;
                    begin
                        RecField.reset;
                        RecField.SetRange(TableNo, 60002);
                        RecField.SetFilter(Type, '%1|%2', RecField.Type::Date, RecField.Type::DateTime);
                        Clear(PageFieldSelection);
                        PageFieldSelection.LookupMode(true);
                        PageFieldSelection.SetRecord(RecField);
                        PageFieldSelection.SetTableView(RecField);
                        if PageFieldSelection.RunModal() = Action::LookupOK then begin
                            PageFieldSelection.GetRecord(RecField);
                            Rec."SMK Target Date Field No." := RecField."No.";
                            Rec.Modify();
                        end;
                    end;
                }
                field(SMKTargetDateFieldCaption2; Rec."SMK Target Date Caption")
                {
                    ApplicationArea = All;
                    Editable = false;
                    Caption = 'Caption Field วันที่เป้าหมาย';
                }
                field(SMKBeforeStartDate2; Rec."SMK Before Start Date")
                {
                    ApplicationArea = All;
                    Caption = 'วันที่เริ่ม Period ตั้งต้น';
                }
                field(SMKBeforeEndDate2; Rec."SMK Before End Date")
                {
                    ApplicationArea = All;
                    Caption = 'วันที่สิ้นสุด Period ตั้งต้น';
                }
                field(SMKAfterStartDate2; Rec."SMK After Start Date")
                {
                    ApplicationArea = All;
                    Caption = 'วันที่เริ่ม Period เป้าหมาย';
                }
                field(SMKAfterEndDate2; Rec."SMK After End Date")
                {
                    ApplicationArea = All;
                    Caption = 'วันที่สิ้นสุด Period เป้าหมาย';
                }
                part(PartSMKBenefitLossRatio; "SMK Period Promo. Bene. Ratio")
                {
                    Caption = 'ผลตอบแทนตามผลต่างสินไหม';
                    SubPageLink = "SMK Promotion Code" = field("SMK Promotion Code");
                    ApplicationArea = All;
                }
            }
        }
    }
}