page 60017 "SMK Period Promo. Setup"
{
    SourceTable = "SMK Promo. Period. Setup";
    PageType = List;
    CardPageId = "SMK Period Promo. Card";
    layout
    {
        area(Content)
        {
            repeater(Content1)
            {
                Caption = 'ทั่วไป';
                field(SMKPromotionCode; Rec."SMK Promotion Code")
                {
                    ApplicationArea = All;
                    Caption = 'รหัสโปรโมชั่น';
                }
                field(SMKDescription; Rec."SMK Description")
                {
                    ApplicationArea = All;
                    Caption = 'รายละเอียด';
                }
                field(SMKStartingDate; Rec."SMK Starting Date")
                {
                    ApplicationArea = All;
                    Caption = 'วันที่เริ่ม';
                }
                field(SMKEndingDate; Rec."SMK Ending Date")
                {
                    ApplicationArea = All;
                    Caption = 'วันที่สิ้นสุด';
                }
                field(SMKDisable; Rec."SMK Disable")
                {
                    ApplicationArea = All;
                    Caption = 'ยกเลิก';
                }
                field(SMKPromotionType; Rec."SMK Promotion Type")
                {
                    ApplicationArea = All;
                    Caption = 'รูปแบบโปรโมชั่น';
                }
                field(SMKApproveStatus; Rec."SMK Approve Status")
                {
                    ApplicationArea = All;
                    Caption = 'สถานะการอนุมัติ';
                }
                field(SMKCreateDate; Rec."SMK Create Date")
                {
                    ApplicationArea = All;
                    Caption = 'วันที่สร้าง';
                }
                field(SMKCreateUser; Rec."SMK Create User")
                {
                    ApplicationArea = All;
                    Caption = 'ผู้สร้าง';
                }
            }
        }
    }
}