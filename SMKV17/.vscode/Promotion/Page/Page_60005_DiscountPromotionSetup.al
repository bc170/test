page 60005 "SMK Disc. Promo. Setups"
{
    SourceTable = "SMK Promo. Disc. Setup";
    PageType = List;
    ApplicationArea = All;
    UsageCategory = Lists;
    CardPageId = "SMK Disc. Promo. Card";
    Caption = 'Discount Promotion Setups';
    layout
    {
        area(Content)
        {
            repeater("Content1")
            {
                field("SMK Promotion Code"; Rec."SMK Promotion Code")
                {
                    Caption = 'รหัสโปรโมชั่น';
                    ApplicationArea = All;
                }
                field("SMK Description"; Rec."SMK Description")
                {
                    Caption = 'รายละเอียด';
                    ApplicationArea = All;
                }
                field("SMK Discount %"; Rec."SMK Discount %")
                {
                    Caption = 'ส่วนลด %';
                    ApplicationArea = All;
                }
                field("SMK Discount Amount"; Rec."SMK Discount Amount")
                {
                    Caption = 'ส่วนลดจำนวนเงิน';
                    ApplicationArea = All;
                }
                field("SMK Starting Date"; Rec."SMK Starting Date")
                {
                    Caption = 'วันที่เริ่ม';
                    ApplicationArea = All;
                }
                field("SMK Ending Date"; Rec."SMK Ending Date")
                {
                    Caption = 'วันที่สิ้นสุด';
                    ApplicationArea = All;
                }
                field("SMK Disable"; Rec."SMK Disable")
                {
                    Caption = 'ยกเลิก';
                    ApplicationArea = All;
                }
                field("SMK Create Date"; Rec."SMK Create Date")
                {
                    Caption = 'วันที่สร้าง';
                    ApplicationArea = All;
                }
                field("SMK Create User"; Rec."SMK Create User")
                {
                    Caption = 'ผู้สร้าง';
                    ApplicationArea = All;
                }
                field("SMK Approve Status"; Rec."SMK Approve Status")
                {
                    Caption = 'สถานะการอนุมัติ';
                    ApplicationArea = All;
                }
            }
        }
    }
    actions
    {
        area(Processing)
        {
            action("SMK Clear Workflow")
            {
                ApplicationArea = All;
                Caption = 'Clear Workflow';
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                Image = ClearLog;
                trigger OnAction()
                var
                    LWorkFlow: Record Workflow;
                    LWorkFlowStep: Record "Workflow Step";
                    Window: Dialog;
                begin

                    LWorkFlow.reset;
                    Window.Open('Code : ###1###');
                    if LWorkFlow.FindSet() then begin
                        repeat
                            Window.Update(1, LWorkFlow.Code);
                            LWorkFlow.Delete(true);
                        until LWorkFlow.Next() = 0;
                    end;
                    Window.Close();
                    //LWorkFlowStep.reset;
                    //LWorkFlowStep.DeleteAll(false);
                end;
            }
            action("SMK Clear Template")
            {
                ApplicationArea = All;
                Caption = 'Clear Template';
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                Image = ClearLog;
                trigger OnAction()
                var
                    LWorkFlow: Record Workflow;
                    LWorkFlowStep: Record "Workflow Step";
                    Window: Dialog;
                begin

                    LWorkFlow.reset;
                    Window.Open('Code : ###1###');
                    if LWorkFlow.FindSet() then begin
                        repeat
                            Window.Update(1, LWorkFlow.Code);
                            LWorkFlow.Template := false;
                            LWorkFlow.Modify(false)
                        until LWorkFlow.Next() = 0;
                    end;
                    Window.Close();
                    //LWorkFlowStep.reset;
                    //LWorkFlowStep.DeleteAll(false);
                end;
            }
        }
    }
}