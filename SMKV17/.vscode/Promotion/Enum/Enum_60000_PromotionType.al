Enum 60000 "SMK Period Promotion Type"
{
    value(1; "SMK Benefit by Policy") { Caption = 'Benefit by Policy'; }
    value(2; "SMK Benefit by Amount") { Caption = 'Benefit by Amount'; }
    value(3; "SMK Benefit by Policy&Amount") { Caption = 'Benefit by Policy&Amount'; }
    value(4; "SMK Benefit by Growthup") { Caption = 'Benefit by Growthup'; }
    value(5; "SMK Benefit by Loss Ratio") { Caption = 'Benefit by Loss Ratio'; }
}