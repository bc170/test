page 60041 "SMK Bank Entry"
{
    SourceTable = "SMK Interface Bank Entry";
    PageType = List;
    layout
    {
        area(Content)
        {
            repeater(Content1)
            {
                field("SMK Import DateTime"; Rec."SMK Import DateTime")
                {
                    ApplicationArea = All;
                }
                field("SMK Ref. 1"; Rec."SMK Ref. 1")
                {
                    ApplicationArea = All;
                }
                field("SMK Ref. 2"; Rec."SMK Ref. 2")
                {
                    ApplicationArea = All;
                }
                field("SMK Ref. 3"; Rec."SMK Ref. 3")
                {
                    ApplicationArea = All;
                }
                field("SMK Amount"; Rec."SMK Amount")
                {
                    ApplicationArea = All;
                }
                field("SMK Orders Amount"; Rec."SMK Verify Collector Amt.")
                {
                    ApplicationArea = All;
                }
                field("SMK Bank Ledg. Amount"; Rec."SMK Verify Finance Amt.")
                {
                    ApplicationArea = All;
                }
            }
        }
    }

    /// <summary> 
    /// Description for SMK SetFilter.
    /// </summary>
    /// <param name="PTransactionType">Parameter of type Option " ","Transfer","Bill Payment".</param>
    /// <param name="PBankCode">Parameter of type Code[20].</param>
    procedure "SMK SetFilter"(PTransactionType: Integer; PBankCode: Code[20])
    begin
        Rec.FilterGroup(0);
        if PTransactionType <> 0 then
            Rec.SetRange("SMK Transaction Type", PTransactionType);
        if PBankCode <> '' then
            Rec.SetRange("SMK Bank Code", PBankCode);
        Rec.FilterGroup(2);
    end;

    var
        OrderAmountStyle: Text[50];
        BankLedgAmount: Text[50];

}