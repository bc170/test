table 60029 "SMK Reconcile Match Entry"
{
    fields
    {
        field(1; "SMK Match Entry No."; Integer)
        {
            DataClassification = ToBeClassified;
        }
        field(2; "SMK Bank Entry No."; Integer)
        {
            DataClassification = ToBeClassified;
        }
        field(3; "SMK Transaction Type"; Option)
        {
            DataClassification = ToBeClassified;
            OptionMembers = " ","Collector","Finance","Accounting";
        }
        field(4; "SMK Transaction Record Id"; RecordId)
        {
            DataClassification = ToBeClassified;
        }
        field(5; "SMK Match Amount"; Decimal)
        {
            DataClassification = ToBeClassified;
        }
    }
    keys
    {
        key(PrimaryKey; "SMK Match Entry No.")
        {
            Clustered = true;
        }
    }
}