table 60028 "SMK Posted Bank Entry"
{
    fields
    {
        field(1; "SMK Entry No."; Integer)
        {
            DataClassification = ToBeClassified;
        }
        field(2; "SMK Bank Code"; Code[20])
        {
            DataClassification = ToBeClassified;
        }
        field(3; "SMK Bank Name"; Text[100])
        {
            DataClassification = ToBeClassified;
        }
        field(4; "SMK Ref. 1"; Code[20])
        {
            DataClassification = ToBeClassified;
        }
        field(5; "SMK Ref. 2"; Code[20])
        {
            DataClassification = ToBeClassified;
        }
        field(6; "SMK Ref. 3"; Code[20])
        {
            DataClassification = ToBeClassified;
        }
        field(7; "SMK Amount"; Decimal)
        {
            DataClassification = ToBeClassified;
        }
        field(8; "SMK Fee"; Decimal)
        {
            DataClassification = ToBeClassified;
        }
    }
    keys
    {
        key(PrimaryKey; "SMK Entry No.")
        {
            Clustered = true;
        }
    }
}