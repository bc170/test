table 60027 "SMK Interface Bank Entry"
{
    fields
    {
        field(1; "SMK Entry No."; Integer)
        {
            DataClassification = ToBeClassified;
        }
        field(2; "SMK Bank Code"; Code[20])
        {
            DataClassification = ToBeClassified;
        }
        field(3; "SMK Bank Name"; Text[100])
        {
            DataClassification = ToBeClassified;
        }
        field(4; "SMK Ref. 1"; Code[20])
        {
            DataClassification = ToBeClassified;
        }
        field(5; "SMK Ref. 2"; Code[20])
        {
            DataClassification = ToBeClassified;
        }
        field(6; "SMK Ref. 3"; Code[20])
        {
            DataClassification = ToBeClassified;
        }
        field(7; "SMK Amount"; Decimal)
        {
            DataClassification = ToBeClassified;
        }
        field(8; "SMK Fee"; Decimal)
        {
            DataClassification = ToBeClassified;
        }
        field(9; "SMK Import Date"; Date)
        {
            DataClassification = ToBeClassified;
        }
        field(10; "SMK Import Time"; Time)
        {
            DataClassification = ToBeClassified;
        }
        field(11; "SMK Import DateTime"; DateTime)
        {
            DataClassification = ToBeClassified;
        }
        field(12; "SMK Transaction Type"; Option)
        {
            DataClassification = ToBeClassified;
            OptionMembers = " ","Transfer","Bill Payment","Credit Card","Third Party";
        }
        field(13; "SMK Entry Type"; Option)
        {
            DataClassification = ToBeClassified;
            OptionMembers = " ","Header","Detail";
        }
        field(14; "SMK Verify Collector Amt."; Decimal)
        {
            FieldClass = FlowField;
            CalcFormula = sum("SMK Reconcile Match Entry"."SMK Match Amount" where("SMK Bank Entry No." = field("SMK Entry No."), "SMK Transaction Type" = const(Collector)));
            Editable = false;
        }
        field(15; "SMK Verify Finance Amt."; Decimal)
        {
            FieldClass = FlowField;
            CalcFormula = sum("SMK Reconcile Match Entry"."SMK Match Amount" where("SMK Bank Entry No." = field("SMK Entry No."), "SMK Transaction Type" = const(Finance)));
            Editable = false;
        }
        field(16; "SMK Bank Branch"; Code[10])
        {
            DataClassification = ToBeClassified;
        }
        field(17; "SMK Bank Terminal"; Code[10])
        {
            DataClassification = ToBeClassified;
        }
        field(18; "SMK Bank Channel"; Code[20])
        {
            DataClassification = ToBeClassified;
        }
        field(19; "SMK Bank Transaction Type"; Text[20])
        {
            DataClassification = ToBeClassified;
        }
        field(20; "SMK Cheque No."; Text[20])
        {
            DataClassification = ToBeClassified;
        }
        field(21; "SMK Deposit Amount"; Decimal)
        {
            DataClassification = ToBeClassified;
        }
        field(22; "SMK Withdraw Amount"; Decimal)
        {
            DataClassification = ToBeClassified;
        }
        field(23; "SMK Bank Acc. Rem. Amt."; Decimal)
        {
            DataClassification = ToBeClassified;
        }
        field(24; "SMK Transction Person Name"; Text[100])
        {
            DataClassification = ToBeClassified;
        }
        field(25; "SMK Credit Card No."; Text[20])
        {
            DataClassification = ToBeClassified;
        }
        field(26; "SMK Verify Accounting Amt."; Decimal)
        {
            FieldClass = FlowField;
            CalcFormula = sum("SMK Reconcile Match Entry"."SMK Match Amount" where("SMK Bank Entry No." = field("SMK Entry No."), "SMK Transaction Type" = const(Accounting)));
            Editable = false;
        }
    }
    keys
    {
        key(PrimaryKey; "SMK Entry No.")
        {
            Clustered = true;
        }
    }
}