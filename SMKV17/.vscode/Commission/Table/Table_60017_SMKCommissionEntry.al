table 60017 "SMK Commission Entry"
{
    DataClassification = CustomerContent;

    fields
    {
        field(1; "Order No."; COde[50])
        {
            DataClassification = CustomerContent;
        }
        field(2; "Policy No."; Code[50])
        {
            DataClassification = CustomerContent;
        }
        field(3; "Standard Commission %"; Decimal)
        {
            DataClassification = CustomerContent;
        }
        field(4; "OVR Commission %"; Decimal)
        {
            DataClassification = CustomerContent;
        }
        field(5; "Commission Amount"; Decimal)
        {
            DataClassification = CustomerContent;
        }
        field(6; "Std. Commission VAT %"; Decimal)
        {
            DataClassification = CustomerContent;
        }
        field(7; "Std. Commission TAX %"; Decimal)
        {
            DataClassification = CustomerContent;
        }
        field(8; "OVR Commission VAT %"; Decimal)
        {
            DataClassification = CustomerContent;
        }
        field(9; "OVR Commission TAX %"; Decimal)
        {
            DataClassification = CustomerContent;
        }
        field(10; "Discount Amount"; Decimal)
        {
            DataClassification = CustomerContent;
        }
        field(11; "Summary Commission"; Decimal)
        {
            DataClassification = CustomerContent;
        }
    }
}