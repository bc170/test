table 60016 "SMK Commission Policy"
{
    DataClassification = CustomerContent;

    fields
    {
        field(1; "Agent Filter"; text[200])
        {
            Caption = 'ตัวแทน';
            DataClassification = CustomerContent;

        }
        field(2; "Product"; Text[20])
        {
            caption = 'ระบบงาน';
            DataClassification = CustomerContent;
            TableRelation = "Dimension Value".Code where("Dimension Code" = const('PRODUCT'));
        }
        field(3; "Agent Card Type"; Option)
        {
            OptionMembers = " ","มีบัตรนายหน้า","ไม่มีบัตรนายหน้า";
            DataClassification = CustomerContent;
        }
        field(4; "Agent Card Subtype"; Option)
        {

            OptionMembers = " ","บัตรปรกติ","วงเงินบัตรเต็ม (Inhouse)","ใช้บัตรลูกทีม","ไม่ใช้บัตร แต่ใช้บัตรลูกทีม (Inhouse)","บัตรหมดอายุ (Inhouse)","ไม่ใช้บัตร ไม่เอาค่าคอมมาตรฐาน","ไม่ใช้บัตร เอาค่าคอมมาตรฐาน (Inhouse)";
            DataClassification = CustomerContent;
        }
        field(5; "Agent Card Status"; Option)
        {
            OptionMembers = " ","Y","S","N","X","E";
            DataClassification = CustomerContent;
        }
        field(6; "Vat Register"; Boolean)
        {

            DataClassification = CustomerContent;
        }
        field(7; "Tax Register"; Boolean)
        {

            DataClassification = CustomerContent;
        }
        field(8; "Vat %"; Decimal)
        {

            DataClassification = CustomerContent;
        }
        field(9; "Tax %"; Decimal)
        {

            DataClassification = CustomerContent;
        }
        field(10; "Tax % (SMK)"; Decimal)

        {
            DataClassification = CustomerContent;
        }
        field(11; "Standard Commission %"; Decimal)
        {
            DataClassification = CustomerContent;
        }
        field(12; "OVR Commission %"; Decimal)

        {
            DataClassification = CustomerContent;
        }
        field(13; "Commission Amount"; Decimal)
        {

            DataClassification = CustomerContent;
        }
        field(14; "Standard Commission Vat %"; Decimal)
        {
            DataClassification = CustomerContent;
        }
        field(15; "Standard Commission Tax %"; Decimal)
        {
            DataClassification = CustomerContent;
        }
        field(16; "OVR Commission VAT"; Decimal)
        {
            DataClassification = CustomerContent;
        }
        field(17; "OVR Commission Tax"; Decimal)
        {
            DataClassification = CustomerContent;
        }
    }

    keys
    {
        key(Key1; "Agent Filter", "Product", "Agent Card Type", "Agent Card Subtype", "Agent Card Status", "Vat Register", "Tax Register", "Vat %", "Tax %", "Tax % (SMK)")
        {
            Clustered = true;
        }
        // key(Key2; "Product")
        // {

        // }
        // key(Key3; "Agent Card Type")
        // {

        // }
        // key(Key4; "Agent Card Subtype")
        // {

        // }
        // key(Key5; "Agent Card Status")
        // {

        // }
        // key(Key6; "Vat Register")
        // {

        // }
        // key(Key7; "Tax Register")
        // {

        // }
        // key(Key8; "Vat %")
        // {

        // }
        // key(Key9; "Tax %")
        // {

        // }
        // key(Key10; "Tax % (SMK)")
        // {

        // }
    }
}