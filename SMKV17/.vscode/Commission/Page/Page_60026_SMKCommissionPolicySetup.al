page 60026 "SMK Commission Policy Setup"
{
    PageType = List;
    ApplicationArea = All;
    UsageCategory = Lists;
    SourceTable = "SMK Commission Policy";
    Caption = 'Commission Policy Setup';
    DeleteAllowed = true;
    ModifyAllowed = true;
    InsertAllowed = true;
    SourceTableTemporary = false;

    layout
    {
        area(Content)
        {
            repeater("Lines")
            {
                field("Agent Filter"; Rec."Agent Filter")
                {
                    ApplicationArea = All;

                }
                field("Product"; Rec."Product")
                {
                    ApplicationArea = all;
                }
                field("Agent Card Type"; Rec."Agent Card Type")
                {
                    ApplicationArea = all;
                }
                field("Agent Card Subtype"; Rec."Agent Card Subtype")
                {
                    ApplicationArea = All;
                }
                field("Agent Card Status"; Rec."Agent Card Status")
                {
                    ApplicationArea = All;
                }
                field("Vat Register"; Rec."Vat Register")
                {
                    ApplicationArea = All;
                }
                field("Tax Register"; Rec."Tax Register")
                {
                    ApplicationArea = All;
                }
                field("Vat %"; Rec."Vat %")
                {
                    ApplicationArea = All;
                }
                field("Tax %"; Rec."Tax %")
                {
                    ApplicationArea = All;
                }
                field("Tax % (SMK)"; Rec."Tax % (SMK)")
                {
                    ApplicationArea = all;
                }
                field("Standard Commission %"; Rec."Standard Commission %")
                {
                    ApplicationArea = all;
                }
                field("OVR Commission %"; Rec."OVR Commission %")
                {
                    ApplicationArea = all;
                }
                field("Commission Amount"; Rec."Commission Amount")
                {
                    ApplicationArea = all;
                }
                field("Standard Commission Vat %"; Rec."Standard Commission Vat %")
                {
                    ApplicationArea = all;
                }
                field("Standard Commission Tax %"; Rec."Standard Commission Tax %")
                {
                    ApplicationArea = all;
                }
                field("OVR Commission VAT"; Rec."OVR Commission VAT")
                {
                    ApplicationArea = all;
                }
                field("OVR Commission Tax"; Rec."OVR Commission Tax")
                {
                    ApplicationArea = all;
                }
            }
        }
    }
}