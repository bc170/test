page 60027 "SMK Commission Entry"
{
    PageType = List;
    ApplicationArea = All;
    Caption = 'Commission Entry';
    UsageCategory = Lists;
    SourceTable = "SMK Commission Entry";
    DeleteAllowed = true;
    ModifyAllowed = true;
    InsertAllowed = true;
    SourceTableTemporary = false;

    layout
    {
        area(Content)
        {
            repeater(Control1)
            {
                field("Order No."; Rec."Order No.")
                {
                    ApplicationArea = All;
                }
                field("Policy No."; Rec."Policy No.")
                {
                    ApplicationArea = All;
                }
                field("Standard Commission %"; Rec."Standard Commission %")
                {
                    ApplicationArea = All;
                }
                field("OVR Commission %"; Rec."OVR Commission %")
                {
                    ApplicationArea = All;
                }
                field("Std. Commission VAT %"; Rec."Std. Commission VAT %")
                {
                    ApplicationArea = All;
                }
                field("Std. Commission TAX %"; Rec."Std. Commission TAX %")
                {
                    ApplicationArea = All;
                }
                field("OVR Commission VAT %"; Rec."OVR Commission VAT %")
                {
                    ApplicationArea = All;
                }
                field("OVR Commission TAX %"; Rec."OVR Commission TAX %")
                {
                    ApplicationArea = All;
                }
                field("Discount Amount"; Rec."Discount Amount")
                {
                    ApplicationArea = All;
                }
                field("Summary Commission"; Rec."Summary Commission")
                {
                    ApplicationArea = All;
                }
            }
        }
    }
}