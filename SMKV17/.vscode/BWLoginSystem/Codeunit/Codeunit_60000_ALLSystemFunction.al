codeunit 60000 "BW ALLSystemFunction"
{
    [EventSubscriber(ObjectType::Codeunit, Codeunit::LogInManagement, 'OnAfterLogInStart', '', false, false)]
    /// <summary> 
    /// Description for BW OnAfterLogInStart.
    /// </summary>
    local procedure "BW OnAfterLogInStart"()
    var
        PageConfirmPass: Page "BW ConfirmPassword";
    begin

        CLEAR(PageConfirmPass);
        Commit();
        PageConfirmPass.RunModal();
        CLEAR(PageConfirmPass);

    end;

    [EventSubscriber(ObjectType::Table, Database::"Invoice Post. Buffer", 'OnAfterInvPostBufferPrepareSales', '', TRUE, TRUE)]

    /// <summary> 
    /// Description for BW InvoiceBufferSales.
    /// </summary>
    /// <param name="InvoicePostBuffer">Parameter of type Record "Invoice Post. Buffer".</param>
    /// <param name="SalesLine">Parameter of type Record "Sales Line".</param>
    local procedure "BW InvoiceBufferSales"(var InvoicePostBuffer: Record "Invoice Post. Buffer"; var SalesLine: Record "Sales Line")
    var
        SalesHeader: Record "Sales Header";
        VendCust: Record "BWK Customer & Vendor Branch";

    begin
        IF SalesHeader.GET(SalesLine."Document Type", SalesLine."Document No.") THEN BEGIN
            InvoicePostBuffer."BW Cilent Computer Name" := SalesHeader."BW Cilent Computer Name";
            InvoicePostBuffer."BW User Name" := SalesHeader."BW User Name";
        END;
    end;

    [EventSubscriber(ObjectType::Table, Database::"Invoice Post. Buffer", 'OnAfterInvPostBufferPreparePurchase', '', true, true)]
    /// <summary> 
    /// Description for BW InvoiceBufferPurchase.
    /// </summary>
    /// <param name="InvoicePostBuffer">Parameter of type Record "Invoice Post. Buffer".</param>
    /// <param name="PurchaseLine">Parameter of type Record "Purchase Line".</param>
    local procedure "BW InvoiceBufferPurchase"(var InvoicePostBuffer: Record "Invoice Post. Buffer"; var PurchaseLine: Record "Purchase Line")
    var
        PurchHeader: Record "Purchase Header";
        VendCust: Record "BWK Customer & Vendor Branch";
    begin
        PurchHeader.GET(PurchaseLine."Document Type", PurchaseLine."Document No.");
        InvoicePostBuffer."BW Cilent Computer Name" := PurchHeader."BW Cilent Computer Name";
        InvoicePostBuffer."BW User Name" := PurchHeader."BW User Name";
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Item Jnl.-Post Line", 'OnAfterInitItemLedgEntry', '', true, true)]
    /// <summary> 
    /// Description for BW AfterInitItemLedgEntry.
    /// </summary>
    /// <param name="ItemJournalLine">Parameter of type Record "Item Journal Line".</param>
    /// <param name="NewItemLedgEntry">Parameter of type Record "Item Ledger Entry".</param>
    local procedure "BW AfterInitItemLedgEntry"(ItemJournalLine: Record "Item Journal Line"; var NewItemLedgEntry: Record "Item Ledger Entry")
    begin
        NewItemLedgEntry."BW Cilent Computer Name" := ItemJournalLine."BW Cilent Computer Name";
        NewItemLedgEntry."BW User Name" := ItemJournalLine."BW User Name";
    end;

    [EventSubscriber(ObjectType::Table, Database::"Item Journal Line", 'OnAfterCopyItemJnlLineFromSalesHeader', '', true, true)]
    /// <summary> 
    /// Description for BW AfterCopyItemJnlLineFromSalesHeader.
    /// </summary>
    /// <param name="SalesHeader">Parameter of type Record "Sales Header".</param>
    /// <param name="ItemJnlLine">Parameter of type Record "Item Journal Line".</param>
    local procedure "BW AfterCopyItemJnlLineFromSalesHeader"(SalesHeader: Record "Sales Header"; var ItemJnlLine: Record "Item Journal Line")
    begin
        ItemJnlLine."BW Cilent Computer Name" := SalesHeader."BW Cilent Computer Name";
        ItemJnlLine."BW User Name" := SalesHeader."BW User Name";
    end;

    [EventSubscriber(ObjectType::Table, Database::"Item Journal Line", 'OnAfterCopyItemJnlLineFromPurchHeader', '', true, true)]
    /// <summary> 
    /// Description for BW AfterCopyItemJnlLineFromPurchHeader.
    /// </summary>
    /// <param name="PurchHeader">Parameter of type Record "Purchase Header".</param>
    /// <param name="ItemJnlLine">Parameter of type Record "Item Journal Line".</param>
    local procedure "BW AfterCopyItemJnlLineFromPurchHeader"(PurchHeader: Record "Purchase Header"; var ItemJnlLine: Record "Item Journal Line")
    begin
        ItemJnlLine."BW Cilent Computer Name" := PurchHeader."BW Cilent Computer Name";
        ItemJnlLine."BW User Name" := PurchHeader."BW User Name";
    end;

    [EventSubscriber(ObjectType::Table, Database::"Gen. Journal Line", 'OnAfterCopyGenJnlLineFromInvPostBuffer', '', true, true)]
    /// <summary> 
    /// Description for BW CopyHeaderFromInvoiceBuff.
    /// </summary>
    /// <param name="InvoicePostBuffer">Parameter of type Record "Invoice Post. Buffer".</param>
    /// <param name="GenJournalLine">Parameter of type Record "Gen. Journal Line".</param>
    local procedure "BW CopyHeaderFromInvoiceBuff"(InvoicePostBuffer: Record "Invoice Post. Buffer"; var GenJournalLine: Record "Gen. Journal Line")
    begin
        GenJournalLine."BW Cilent Computer Name" := InvoicePostBuffer."BW Cilent Computer Name";
        GenJournalLine."BW User Name" := InvoicePostBuffer."BW User Name";
    end;


    [EventSubscriber(ObjectType::Table, Database::"VAT Entry", 'OnAfterCopyFromGenJnlLine', '', true, true)]
    /// <summary> 
    /// Description for BW CopyVatFromGenLine.
    /// </summary>
    /// <param name="GenJournalLine">Parameter of type Record "Gen. Journal Line".</param>
    /// <param name="VATEntry">Parameter of type Record "VAT Entry".</param>
    local procedure "BW CopyVatFromGenLine"(GenJournalLine: Record "Gen. Journal Line"; var VATEntry: Record "VAT Entry")
    begin
        VATEntry."BW Cilent Computer Name" := GenJournalLine."BW Cilent Computer Name";
        VATEntry."BW User Name" := GenJournalLine."BW User Name";
    end;


    [EventSubscriber(ObjectType::Table, Database::"Gen. Journal Line", 'OnAfterCopyGenJnlLineFromPurchHeader', '', true, true)]
    /// <summary> 
    /// Description for BW CopyHeaderFromPurchaseHeader.
    /// </summary>
    /// <param name="PurchaseHeader">Parameter of type Record "Purchase Header".</param>
    /// <param name="GenJournalLine">Parameter of type Record "Gen. Journal Line".</param>
    local procedure "BW CopyHeaderFromPurchaseHeader"(PurchaseHeader: Record "Purchase Header"; var GenJournalLine: Record "Gen. Journal Line")
    begin
        GenJournalLine."BW Cilent Computer Name" := PurchaseHeader."BW Cilent Computer Name";
        GenJournalLine."BW User Name" := PurchaseHeader."BW User Name";
    end;

    [EventSubscriber(ObjectType::Table, Database::"Gen. Journal Line", 'OnAfterCopyGenJnlLineFromSalesHeader', '', true, true)]
    /// <summary> 
    /// Description for BW CopyHeaderFromSalesHeader.
    /// </summary>
    /// <param name="SalesHeader">Parameter of type Record "Sales Header".</param>
    /// <param name="GenJournalLine">Parameter of type Record "Gen. Journal Line".</param>
    local procedure "BW CopyHeaderFromSalesHeader"(SalesHeader: Record "Sales Header"; var GenJournalLine: Record "Gen. Journal Line")
    begin
        GenJournalLine."BW Cilent Computer Name" := SalesHeader."BW Cilent Computer Name";
        GenJournalLine."BW User Name" := SalesHeader."BW User Name";
    end;

    [EventSubscriber(ObjectType::Table, Database::"G/L Entry", 'OnAfterCopyGLEntryFromGenJnlLine', '', false, false)]
    /// <summary> 
    /// Description for BW OnAfterCopyGLEntryFromGenJnlLine.
    /// </summary>
    /// <param name="GenJournalLine">Parameter of type Record "Gen. Journal Line".</param>
    /// <param name="GLEntry">Parameter of type Record "G/L Entry".</param>
    local procedure "BW OnAfterCopyGLEntryFromGenJnlLine"(var GenJournalLine: Record "Gen. Journal Line"; var GLEntry: Record "G/L Entry")
    begin
        GLEntry."BW Cilent Computer Name" := GenJournalLine."BW Cilent Computer Name";
        GLEntry."BW User Name" := GenJournalLine."BW User Name";
    end;
}