Table 60000 "BW UserPasswordConfirm"
{
    Caption = 'UserPasswordConfirm';
    DataPerCompany = false;
    fields
    {


        field(1; "BW USER Name"; code[30])
        {
            Caption = 'BW Username';
            DataClassification = CustomerContent;
        }
        field(3; "BW Password"; Text[100])
        {
            Caption = 'BW Password';
            DataClassification = CustomerContent;
        }

    }
    keys
    {
        key(PK1; "BW USER Name")
        {
            Clustered = true;
        }
    }
}