table 60001 "BW LogInformation"
{
    Caption = 'LogInformation';
    fields
    {
        field(1; "BW Session ID"; Integer)
        {
            Caption = 'BW Session ID';
            DataClassification = SystemMetadata;
            Editable = false;
        }
        field(2; "BW Server Instance ID"; Integer)
        {
            Caption = 'BW Server Instance ID';
            //TableRelation = "Server Instance"."Server Instance ID";
        }
        field(3; "BW USER Name"; Code[30])
        {
            Caption = 'BW Username';
            DataClassification = SystemMetadata;
            Editable = false;
        }
        field(4; "BW Cilent Computer Name"; Text[250])
        {
            Caption = 'BW Cilent Computer Name';
            DataClassification = SystemMetadata;
            Editable = false;
        }
        field(5; "BW Login Datetime"; DateTime)
        {
            Caption = 'BW Login Datetime';
            DataClassification = SystemMetadata;
            Editable = false;
        }
        field(6; "BW USER ID"; Code[30])
        {
            Caption = 'BW USER ID';
            DataClassification = SystemMetadata;
            Editable = false;
        }

    }
    keys
    {
        key(PK; "BW Session ID", "BW Server Instance ID")
        {
            Clustered = true;
        }
    }

}