page 60001 "BW ConfirmPassword"
{
    Caption = 'ConfirmPassword';
    PageType = ConfirmationDialog;

    layout
    {
        area(Content)
        {
            //group("BW General")
            //{

            //Caption = 'Login';
            field(UserName;
            UserName)
            {
                ApplicationArea = all;
                Caption = 'User Name';

            }
            field(Password; Password)
            {
                ApplicationArea = all;
                ExtendedDatatype = Masked;
                Caption = 'Password';
            }
        }
        //}
    }


    trigger OnQueryClosePage(CloseAction: Action): Boolean

    var
        Logininfor: Record "BW LogInformation";
        ACtiveSession: Record "Active Session";
        ConfirmPass: Record "BW UserPasswordConfirm";
    begin
        if CloseAction IN [CloseAction::OK, CloseAction::Yes] then begin
            ACtiveSession.GET(ServiceInstanceId(), SessionId());
            ConfirmPass.reset;
            ConfirmPass.SetRange("BW USER Name", UserName);
            ConfirmPass.SetRange("BW Password", Password);
            if ConfirmPass.FindFirst() then begin
                Logininfor.init;
                Logininfor."BW Session ID" := SessionId();
                Logininfor."BW Server Instance ID" := ServiceInstanceId();
                Logininfor."BW USER ID" := UserId;
                Logininfor."BW USER Name" := UserName;
                Logininfor."BW Login Datetime" := CurrentDateTime;
                Logininfor."BW Cilent Computer Name" := ACtiveSession."Client Computer Name";
                Logininfor.Insert();
            end else begin
                ERROR('Username or Password is in correct!');
            end;
        end else begin
            ERROR('must be Login');

        end;
    end;

    var
        UserName: Code[30];
        Password: Text[100];
        LbLogin: Label 'Login';
}