page 60000 "BW UserPassword Setup"
{
    SourceTable = "BW UserPasswordConfirm";
    Caption = 'User Password Setup';
    ApplicationArea = all;
    UsageCategory = Administration;
    PageType = ListPlus;
    DataCaptionFields = "BW USER Name";
    SourceTableTemporary = false;
    layout
    {
        area(Content)
        {
            repeater("BW General")
            {
                Caption = 'Lines';
                field("BW USER Name"; Rec."BW USER Name")
                {
                    ApplicationArea = all;
                }
                field("BW Password"; Rec."BW Password")
                {
                    ApplicationArea = all;
                    ExtendedDatatype = Masked;
                }
            }
        }
    }
}