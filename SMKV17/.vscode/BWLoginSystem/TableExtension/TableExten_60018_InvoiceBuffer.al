tableextension 60018 "BW InvoiceBuffer" extends "Invoice Post. Buffer"
{
    fields
    {
        field(60000; "BW User Name"; Code[30])
        {
            Caption = 'BW Username';
            Editable = false;
        }
        field(60001; "BW Cilent Computer Name"; Text[100])
        {
            Caption = 'BW Cilent Computer Name';
            Editable = false;
        }
    }
    trigger OnInsert()
    var
        LoginInfor: Record "BW LogInformation";
    begin
        if LoginInfor.GET(SessionId(), ServiceInstanceId()) then begin
            "BW User Name" := LoginInfor."BW USER Name";
            "BW Cilent Computer Name" := LoginInfor."BW Cilent Computer Name";
        end;
    end;
}