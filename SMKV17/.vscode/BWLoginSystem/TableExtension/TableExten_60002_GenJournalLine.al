tableextension 60002 "BW GenLinesAllSystem" extends "Gen. Journal Line"
{
    fields
    {
        field(60000; "BW User Name"; Code[30])
        {
            Caption = 'BW Username';
            Editable = false;
        }
        field(60001; "BW Cilent Computer Name"; Text[100])
        {
            Caption = 'BW Cilent Computer Name';
            Editable = false;
        }
        field(70000; "SMK Bank Ref. 1"; Text[20])
        {
            DataClassification = CustomerContent;
        }
        field(70001; "SMK Bank Ref. 2"; Text[20])
        {
            DataClassification = CustomerContent;
        }
        field(70002; "SMK Ref. Policy No."; Integer)
        {
            DataClassification = ToBeClassified;
        }
        field(70003; "SMK Payment Type"; Code[20])
        {
            DataClassification = ToBeClassified;
            TableRelation = "Dimension Value".Code where("Dimension Code" = const('PAYMENTTYPE'));
        }
        field(70004; "SMK Ref. Order No."; Code[20])
        {
            DataClassification = ToBeClassified;
        }
    }
    trigger OnInsert()
    var
        LoginInfor: Record "BW LogInformation";
    begin
        if LoginInfor.GET(SessionId(), ServiceInstanceId()) then begin
            "BW User Name" := LoginInfor."BW USER Name";
            "BW Cilent Computer Name" := LoginInfor."BW Cilent Computer Name";
        end;
    end;
}