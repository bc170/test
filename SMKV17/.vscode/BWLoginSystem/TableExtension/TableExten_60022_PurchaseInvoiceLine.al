tableextension 60022 "BW PurchaseInvoiceLinesALLSys" extends "Purch. Inv. Line"
{
    fields
    {
        field(60000; "BW User Name"; Code[30])
        {
            Caption = 'BW Username';
            Editable = false;
        }
        field(60001; "BW Cilent Computer Name"; Text[100])
        {
            Caption = 'BW Cilent Computer Name';
            Editable = false;
        }
    }

}