tableextension 60001 "BW PurchaseHeaderALLSys" extends "Purchase Header"
{
    fields
    {
        field(60000; "BW User Name"; Code[30])
        {
            Caption = 'BW Username';
            Editable = false;
        }
        field(60001; "BW Cilent Computer Name"; Text[100])
        {
            Caption = 'BW Cilent Computer Name';
            Editable = false;
        }
        field(60002; "SMK Cash Amount"; Decimal)
        {
            Description = 'จำนวนเงินสด';
            DataClassification = CustomerContent;
        }
        field(60003; "SMK Summary Cash Amount in Day"; Decimal)
        {
            Description = 'จำนวนเงินสดที่รับมาในวันนั้น';
            DataClassification = ToBeClassified;
            Editable = false;
        }
        field(60004; "SMK Bank Ref. 1"; Text[20])
        {
            DataClassification = CustomerContent;
        }
        field(60005; "SMK Bank Ref. 2"; Text[20])
        {
            DataClassification = CustomerContent;
        }
        field(60006; "SMK Deposit Date"; Date)
        {
            DataClassification = CustomerContent;
        }
        field(60007; "SMK Front Bank Deposit Lock"; Boolean)
        {
            DataClassification = CustomerContent;
        }
    }
    trigger OnInsert()
    var
        LoginInfor: Record "BW LogInformation";
    begin
        if LoginInfor.GET(SessionId(), ServiceInstanceId()) then begin
            "BW User Name" := LoginInfor."BW USER Name";
            "BW Cilent Computer Name" := LoginInfor."BW Cilent Computer Name";
        end;
    end;
}