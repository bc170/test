tableextension 60012 "BW BillingReceiptHeaderSystem" extends "BWK Billing & Receipt Header"
{
    fields
    {
        field(60000; "BW User Name"; Code[30])
        {
            Caption = 'BW Username';
            Editable = false;
        }
        field(60001; "BW Cilent Computer Name"; Text[100])
        {
            Caption = 'BW Cilent Computer Name';
            Editable = false;
        }
    }
    trigger OnInsert()
    var
        LoginInfor: Record "BW LogInformation";
    begin
        LoginInfor.GET(SessionId(), ServiceInstanceId());
        "BW User Name" := LoginInfor."BW USER Name";
        "BW Cilent Computer Name" := LoginInfor."BW Cilent Computer Name";
    end;
}