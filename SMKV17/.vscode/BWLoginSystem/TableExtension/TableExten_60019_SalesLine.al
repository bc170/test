tableextension 60019 "BW SalesLinesALLSys" extends "Sales Line"
{
    fields
    {
        field(60000; "BW User Name"; Code[30])
        {
            Caption = 'BW Username';
            Editable = false;
        }
        field(60001; "BW Cilent Computer Name"; Text[100])
        {
            Caption = 'BW Cilent Computer Name';
            Editable = false;
        }
        field(60002; "SMK Policy No."; code[20])
        {
            DataClassification = ToBeClassified;
        }
        field(60003; "SMK Ref. Cust. Ledg. Entry No."; Integer)
        {
            DataClassification = ToBeClassified;
        }
    }
    trigger OnInsert()
    var
        LoginInfor: Record "BW LogInformation";
    begin
        if LoginInfor.GET(SessionId(), ServiceInstanceId()) then begin
            "BW User Name" := LoginInfor."BW USER Name";
            "BW Cilent Computer Name" := LoginInfor."BW Cilent Computer Name";
        end;
    end;
}