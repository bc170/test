tableextension 60000 "BW SalesHeaderALLSys" extends "Sales Header"
{
    fields
    {
        field(60000; "BW User Name"; Code[30])
        {
            Caption = 'BW Username';
            Editable = false;
        }
        field(60001; "BW Cilent Computer Name"; Text[100])
        {
            Caption = 'BW Cilent Computer Name';
            Editable = false;
        }
        field(60002; "SMK Receipt Document No."; code[20])
        {
            DataClassification = ToBeClassified;
        }
        field(60003; "SMK Front Receipt Lock"; Boolean)
        {
            DataClassification = ToBeClassified;
        }
        field(60004; "SMK Front Send to RV"; Boolean)
        {
            DataClassification = SystemMetadata;
        }
        field(60005; "SMK Agent No."; Boolean)
        {
            DataClassification = ToBeClassified;
        }
        field(60006; "SMK Agent Type"; Boolean)
        {
            DataClassification = ToBeClassified;
        }
        field(60007; "SMK Department Code"; Code[20])
        {
            DataClassification = ToBeClassified;
            TableRelation = "Dimension Value".Code where("Dimension Code" = const('DEPARTMENT'));
        }
        field(60008; "SMK Department Name"; Text[50])
        {
            FieldClass = FlowField;
            CalcFormula = lookup("Dimension Value".Name where("Dimension Code" = const('DEPARTMENT'), Code = field("SMK Department Code")));
            Editable = false;
        }
        field(60009; "SMK Branch Code"; Code[20])
        {
            DataClassification = ToBeClassified;
            TableRelation = "Dimension Value".Code where("Dimension Code" = const('BRANCH'));
        }
        field(60010; "SMK Branch Name"; Text[50])
        {
            FieldClass = FlowField;
            CalcFormula = lookup("Dimension Value".Name where("Dimension Code" = const('BRANCH'), Code = field("SMK Branch Code")));
            Editable = false;
        }
        field(60011; "SMK Count Policy"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("Sales Line" where("Document Type" = field("Document Type"), "Document No." = field("No.")));
            Editable = false;
        }
        field(60012; "SMK Policy Amount"; Decimal)
        {
            FieldClass = FlowField;
            CalcFormula = Sum("Sales Line".Amount where("Document Type" = field("Document Type"), "Document No." = field("No.")));
            Editable = false;
        }
        field(60013; "SMK Count Pay"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("Sales Comment Line" where("Document Type" = field("Document Type"), "No." = field("No.")));
        }
        field(60014; "SMK Pay Amount"; Decimal)
        {
            FieldClass = FlowField;
            CalcFormula = Sum("Sales Line".Amount where("Document Type" = field("Document Type"), "Document No." = field("No.")));
            Editable = false;
        }
        field(60015; "SMK Receipt From"; Option)
        {
            DataClassification = ToBeClassified;
            OptionMembers = " ","ลูกค้า","ตัวแทน";
        }

    }
    trigger OnInsert()
    var
        LoginInfor: Record "BW LogInformation";
    //MYEnvironment: DotNet 
    begin

        if LoginInfor.GET(SessionId(), ServiceInstanceId()) then begin
            "BW User Name" := LoginInfor."BW USER Name";
            "BW Cilent Computer Name" := LoginInfor."BW Cilent Computer Name";
        end;
    end;
}