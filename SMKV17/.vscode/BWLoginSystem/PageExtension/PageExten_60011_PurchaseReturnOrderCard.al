pageextension 60011 "BW PurchaseReturnOrderCard1" extends "Purchase Return Order"
{
    layout
    {
        addlast(content)
        {
            group("BW User Detail")
            {
                Caption = 'User Detail';
                field("BW User Name"; Rec."BW User Name")
                {
                    ApplicationArea = all;
                }
                field("BW Cilent Computer Name"; Rec."BW Cilent Computer Name")
                {

                    ApplicationArea = all;
                }
            }
        }
    }
}