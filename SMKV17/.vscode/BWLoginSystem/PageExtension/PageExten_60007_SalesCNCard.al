pageextension 60007 "BW SalesCNCard" extends "Sales Credit Memo"
{
    layout
    {
        addlast(content)
        {
            group("BW User Detail")
            {
                Caption = 'User Detail';
                field("BW User Name"; Rec."BW User Name")
                {
                    ApplicationArea = all;
                }
                field("BW Cilent Computer Name"; Rec."BW Cilent Computer Name")
                {

                    ApplicationArea = all;
                }
            }
        }
    }
}