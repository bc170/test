pageextension 60004 "BW SalesQuotesCard" extends "Sales Quote"
{
    layout
    {
        addlast(content)
        {
            group("BW User Detail")
            {
                Caption = 'User Detail';
                field("BW User Name"; Rec."BW User Name")
                {
                    ApplicationArea = all;
                }
                field("BW Cilent Computer Name"; Rec."BW Cilent Computer Name")
                {

                    ApplicationArea = all;
                }
            }
        }
    }
}