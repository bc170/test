pageextension 60003 "BW PurchaseCNCard" extends "Purchase Credit Memo"
{
    layout
    {
        addlast(content)
        {
            group("BW User Detail")
            {
                Caption = 'User Detail';
                field("BW User Name"; Rec."BW User Name")
                {
                    ApplicationArea = all;
                }
                field("BW Cilent Computer Name"; Rec."BW Cilent Computer Name")
                {

                    ApplicationArea = all;
                }
            }
        }
    }
}