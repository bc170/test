pageextension 60000 "BW PRCard" extends "BWK Purchase Requisition Card"
{
    layout
    {
        addlast(content)
        {
            group("BW User Detail")
            {
                Caption = 'User Detail';
                field("BW User Name"; Rec."BW User Name")
                {
                    ApplicationArea = all;
                }
                field("BW Cilent Computer Name"; Rec."BW Cilent Computer Name")
                {

                    ApplicationArea = all;
                }
            }
        }
    }
}