pageextension 60017 "BW SMK Customer Card" extends "Customer Card"
{
    layout
    {
        modify("Credit Limit (LCY)")
        {
            Visible = false;
        }
        modify("Service Zone Code")
        {
            Visible = false;
        }
        modify("Document Sending Profile")
        {
            Visible = false;
        }
        modify(TotalSales2)
        {
            Visible = false;
        }
        modify("CustSalesLCY - CustProfit - AdjmtCostLCY")
        {
            Visible = false;
        }
        modify(AdjCustProfit)
        {
            Visible = false;
        }
        modify(AdjProfitPct)
        {
            Visible = false;
        }
        addafter(Blocked)
        {
            field("SMK Hold"; Rec."SMK Hold")
            {
                ApplicationArea = all;
            }

        }
    }

    actions
    {
        addafter("F&unctions")
        {
            action("BW Setup Credit Limit")
            {
                ApplicationArea = All;
                Caption = 'BW Setup Credit Limit';
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                Image = AuthorizeCreditCard;
                trigger OnAction()
                var
                    PageAgentCreitLimit: Page "SMK Agent Credit Limit";
                begin
                    Clear(PageAgentCreitLimit);
                    PageAgentCreitLimit.SetCust(Rec."No.");
                    PageAgentCreitLimit.RunModal();
                end;
            }
            action(AgentCollateral)
            {
                ApplicationArea = all;
                Caption = 'Agent Collateral';
                Image = Document;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;

                trigger OnAction()
                var
                    PageAgentCollateral: Page "SMK Agent Collateral";
                begin
                    Clear(PageAgentCollateral);
                    PageAgentCollateral.SetCust(Rec."No.");
                    PageAgentCollateral.RunModal();
                end;
            }
            action(AgentCard)
            {
                ApplicationArea = all;
                Caption = 'Agent Card';
                Image = Document;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;

                trigger OnAction()
                var
                    PageAgentCard: Page "SMK Agent Card";
                begin
                    Clear(PageAgentCard);
                    PageAgentCard.SetCust(Rec."No.");
                    PageAgentCard.RunModal();
                end;
            }
            action("Manual Hold")
            {
                ApplicationArea = all;
                Caption = 'Manual Hold';
                Image = Cancel;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                Enabled = (Not Rec."SMK Hold");
                trigger OnAction()
                begin
                    Rec."SMK Hold" := TRUE;
                    Rec.Modify();
                end;
            }
            action("Manual Unhold")
            {
                ApplicationArea = all;
                Caption = 'Manual Unhold';
                Image = Approve;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                Enabled = (Rec."SMK Hold");
                trigger OnAction()
                begin
                    Rec."SMK Hold" := FALSE;
                    Rec.Modify();
                end;
            }

        }

    }

}