tableextension 60051 "SMK Dimension Value" extends "Dimension Value"
{
    fields
    {
        field(60000; "SMK Ref. Prod. Group"; Code[20])
        {
            Caption = 'BW Ref. Prod. Group';
            DataClassification = SystemMetadata;
        }
    }
}