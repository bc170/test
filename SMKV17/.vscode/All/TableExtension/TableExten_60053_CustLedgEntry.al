tableextension 60053 "SMK Cust. Ledg. Entry" extends "Cust. Ledger Entry"
{
    fields
    {
        field(70000; "SMK Ref. Policy No."; code[20])
        {
            DataClassification = ToBeClassified;
        }
        field(70001; "SMK Payment Type"; Code[20])
        {
            DataClassification = ToBeClassified;
            TableRelation = "Dimension Value".Code where("Dimension Code" = const('PAYMENTTYPE'));
        }
        field(70002; "SMK Ref. Order No."; Code[20])
        {
            DataClassification = ToBeClassified;
        }
        field(70003; "SMK Bank Ref. 1"; Text[20])
        {
            DataClassification = CustomerContent;
        }
        field(70004; "SMK Bank Ref. 2"; Text[20])
        {
            DataClassification = CustomerContent;
        }

    }
}