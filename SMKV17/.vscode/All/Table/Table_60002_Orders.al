table 60002 "SMK Orders"
{
    fields
    {
        field(1; "SMK Order No."; Code[20])
        {
            DataClassification = ToBeClassified;
            Description = 'not_id - เลขที่รับแจ้ง';
            Caption = 'เลขที่รับแจ้ง';
        }
        field(2; "SMK Order Type"; Integer)
        {
            DataClassification = ToBeClassified;
            Description = 'not_type - ประเภทงาน';
            Caption = 'ประเภทงาน';
        }
        field(3; "SMK Policy No."; Code[20])
        {
            DataClassification = ToBeClassified;
            Description = 'policy - เลขกรมธรรม์';
            Caption = 'เลขที่กรมธรรม์';
        }
        field(4; "SMK Policy Date"; Date)
        {
            DataClassification = ToBeClassified;
            Description = 'pol_dte - วันที่ผลิต กธ.';
            Caption = 'วันที่ผลิต กธ.';
        }
        field(5; "SMK Year"; Integer)
        {
            DataClassification = ToBeClassified;
            Description = 'yea_no - ปีที่ประกัน';
            Caption = 'ปีที่ประกัน';
        }
        field(6; "SMK Send Date"; Date)
        {
            DataClassification = ToBeClassified;
            Description = 'send_dte - วันที่ฝ่ายผลิตส่ง กธ. ให้ฝ่ายจัดเก็บเบี้ย';
            Caption = 'วันที่ฝ่ายผลิตส่ง กธ. ให้ฝ่ายจัดเก็บเบี้ย';
        }
        field(7; "SMK Col. Receive Date"; Date)
        {
            DataClassification = ToBeClassified;
            Description = 'frepo_dte - วันที่ฝ่ายจัดเก็บเบี้ยรับ กธ. จากฝ่ายผลิต';
            Caption = 'วันที่ฝ่ายจัดเก็บเบี้ยรับ กธ. จากฝ่ายผลิต';
        }
        field(8; "SMK Col. Send Receive Date"; Date)
        {
            DataClassification = ToBeClassified;
            Description = 'fsen_dte - วันที่ฝ่ายจัดเก็บเบี้ยส่ง กธ. ให้ลูกค้า';
            Caption = 'วันที่ฝ่ายจัดเก็บเบี้ยส่ง กธ. ให้ลูกค้า';
        }
        field(9; "SMK Col. Reconcile Date"; Date)
        {
            DataClassification = ToBeClassified;
            Description = 'frec_dte - วันที่ฝ่ายจัดเก็บเบี้ยตัดรับเบี้ย';
            Caption = 'วันที่ฝ่ายจัดเก็บเบี้ยตัดรับเบี้ย';
        }
        field(10; "SMK Policy Status"; Option)
        {
            OptionMembers = "Active","Wait";
            DataClassification = ToBeClassified;
            Description = 'pol_sts - สถานะ กธ.';
            Caption = 'สถานะ กธ.';
        }
        field(11; "SMK End No."; Code[20])
        {
            DataClassification = ToBeClassified;
            Description = 'end_id - เลขสลักหลัง';
            Caption = 'เลขสลักหลัง';
        }
        field(12; "SMK End Type"; Integer)
        {
            DataClassification = ToBeClassified;
            Description = 'end_type - ประเภทสลักหลัก';
            Caption = 'ประเภทสลักหลัง';
        }
        field(13; "SMK Running"; Integer)
        {
            DataClassification = ToBeClassified;
            Description = 'running - ครั้งที่ทำสลักหลัง';
            Caption = 'ครั้งที่ทำสลักหลัง';
        }
        field(14; "SMK End Date"; Date)
        {
            DataClassification = ToBeClassified;
            Description = 'end_dte - วันที่ทำสลักหลัง';
            Caption = 'วันที่ทำสลักหลัง';
        }
        field(15; "SMK Effect End Date"; Date)
        {
            DataClassification = ToBeClassified;
            Description = 'eff_end_dte - วันที่มีผลของสลักหลัก';
            Caption = 'วันที่มีผลของสลักหลัง';
        }
        field(16; "SMK End Year"; Integer)
        {
            DataClassification = ToBeClassified;
            Description = 'cent_end - ปีสลักหลัง';
            Caption = 'ปีสลักหลัง';
        }
        field(17; "SMK Agent No."; Code[20])
        {
            DataClassification = ToBeClassified;
            Description = 'agt_cod - รหัสตัวแทน';
            Caption = 'รหัสตัวแทน';
        }
        field(18; "SMK Old Policy"; Code[20])
        {
            DataClassification = ToBeClassified;
            Description = 'old_pol - เลขกรมธรรม์เดิม';
            Caption = 'เลขกรมธรรม์เดิม';
        }
        field(19; "SMK Std. Commission %"; Decimal)
        {
            DataClassification = ToBeClassified;
            Description = 'mot_com_per - % ค่าคอมฯ มาตรฐาน ตอนกำหนดเลข กธ.';
            Caption = '% ค่าคอมฯ มาตรฐาน ตอนกำหนดเลข กธ.';
        }
        field(20; "SMK Branch"; Code[20])
        {
            DataClassification = ToBeClassified;
            Description = 'branch - สาขากรมธรรม์';
            Caption = 'สาขากรมธรรม์';
        }
        field(21; "SMK Campaign"; Code[20])
        {
            DataClassification = ToBeClassified;
            Description = 'campaign - แคมเปญ';
            Caption = 'แคมเปญ';
        }
        field(22; "SMK Promotion"; Code[20])
        {
            DataClassification = ToBeClassified;
            Description = 'promotion - โปรโมชั่น';
            Caption = 'โปรโมชั่น';
        }
        field(23; "SMK Policy Year"; Integer)
        {
            DataClassification = ToBeClassified;
            Description = 'pol_yea - ปีกธ.';
            Caption = 'ปี กธ.';
        }
        field(24; "SMK License"; Text[10])
        {
            DataClassification = ToBeClassified;
            Description = 'license - ทะเบียนรถ';
            Caption = 'ทะเบียนรถ';
        }
        field(25; "SMK Chasis"; Text[20])
        {
            DataClassification = ToBeClassified;
            Description = 'chasis - เลขคัสซี';
            Caption = 'เลขคัสซี';
        }
        field(26; "SMK Engine"; Text[20])
        {
            DataClassification = ToBeClassified;
            Description = 'engine - เลขเครื่อง';
            Caption = 'เลขเครื่อง';
        }
        field(27; "SMK Broker No."; Text[20])
        {
            DataClassification = ToBeClassified;
            Description = 'broker_cod - รหัสโบรกเกอร์';
            Caption = 'รหัสโบรกเกอร์';
        }
        field(28; "SMK Contract Date"; Date)
        {
            DataClassification = ToBeClassified;
            Description = 'agr_dte - วันทำสัญญา';
            Caption = 'วันทำสัญญา';
        }
        field(29; "SMK Protect Eff. Start Date"; Date)
        {
            DataClassification = ToBeClassified;
            Description = 'eff_fdte - วันเริ่มคุ้มครอง';
            Caption = 'วันเริ่มคุ้มครอง';
        }
        field(30; "SMK Protect Eff. End Date"; Date)
        {
            DataClassification = ToBeClassified;
            Description = 'eff_tdte - วันสิ้นสุดคุ้มครอง';
            Caption = 'วันสิ้นสุดคุ้มครอง';
        }
        field(31; "SMK Account Period Date"; Date)
        {
            DataClassification = ToBeClassified;
            Description = 'dte_acc - วันรอบบัญชี';
            Caption = 'วันรอบบัญชี';
        }
        field(32; "SMK Car Code"; Text[20])
        {
            DataClassification = ToBeClassified;
            Description = 'car_code - รหัสรถ';
            Caption = 'รหัสรถ';
        }
        field(33; "SMK Car Make"; Text[50])
        {
            DataClassification = ToBeClassified;
            Description = 'car_mak - รุ่นรถ';
            Caption = 'รุ่นรถ';
        }
        field(34; "SMK Car Model"; Text[50])
        {
            DataClassification = ToBeClassified;
            Description = 'car_mod - ปีรถ';
            Caption = 'ปีรถ';
        }
        field(35; "SMK Car Group"; Text[50])
        {
            DataClassification = ToBeClassified;
            Description = 'car_group - กลุ่มรถ';
            Caption = 'กลุ่มรถ';
        }
        field(36; "SMK Car Body"; Text[50])
        {
            DataClassification = ToBeClassified;
            Description = 'car_bod - Body';
            Caption = 'Body';
        }
        field(37; "SMK Car Seat"; Integer)
        {
            DataClassification = ToBeClassified;
            Description = 'seat_no - จำนวนที่นั่ง';
            Caption = 'จำนวนที่นั่ง';
        }
        field(38; "SMK Car CC"; Integer)
        {
            DataClassification = ToBeClassified;
            Description = 'car_cc - ขนาด CC';
            Caption = 'ขนาด CC';
        }
        field(39; "SMK Weight"; Decimal)
        {
            DataClassification = ToBeClassified;
            Description = 'weigh - น้ำหนัก';
            Caption = 'น้ำหนัก';
        }
        field(40; "SMK Premium Net"; Decimal)
        {
            DataClassification = ToBeClassified;
            Description = 'pre_net - เบี้ยสุทธิ';
            Caption = 'เบี้ยสุทธิ';
        }
        field(41; "SMK Premium Tax"; Decimal)
        {
            DataClassification = ToBeClassified;
            Description = 'pre_tax - ภาษี';
            Caption = 'ภาษี';
        }
        field(42; "SMK Premium Duty"; Decimal)
        {
            DataClassification = ToBeClassified;
            Description = 'pre_stm - อากร';
            Caption = 'อากร';
        }
        field(43; "SMK Premium Summary"; Decimal)
        {
            DataClassification = ToBeClassified;
            Description = 'pre_grs - เบี้ยรวม';
            Caption = 'เบี้ยรวม';
        }
        field(44; "SMK Policy Type"; Integer)
        {
            DataClassification = ToBeClassified;
            Description = 'mot_typ - ประเภท กธ.';
            Caption = 'ประเภท กธ.';
        }
        field(45; "SMK Receipt Type"; Text[10])
        {
            DataClassification = ToBeClassified;
            Description = 'receive_type - ประเภทใบเสร็จ';
            Caption = 'ประเภทใบเสร็จ';
        }
        field(46; "SMK Receipt No."; Text[10])
        {
            DataClassification = ToBeClassified;
            Description = 'receive_id - เลขที่ใบเสร็จ';
            Caption = 'เลขที่ใบเสร็จ';
        }
        field(47; "SMK Receipt Date"; Date)
        {
            DataClassification = ToBeClassified;
            Description = 'receive_dte - วันที่ใบเสร็จ';
            Caption = 'วันที่ใบเสร็จ';
        }
        field(48; "SMK Customer Name"; Text[100])
        {
            DataClassification = ToBeClassified;
            Description = 'cust_name - ชื่อลูกค้า';
            Caption = 'ชื่อลูกค้า';
        }
        field(49; "SMK Sticker Year"; Integer)
        {
            DataClassification = ToBeClassified;
            Description = 'stic_year - ปีเลข Sticker';
            Caption = 'ปีเลข Sticker';
        }
        field(50; "SMK Sticker No."; Code[20])
        {
            DataClassification = ToBeClassified;
            Description = 'sticker - เลขเครื่องหมาย';
            Caption = 'เลขเครื่องหมาย';
        }
        field(1000; "SMK Already Invoiced"; Boolean)
        {
            DataClassification = ToBeClassified;
            Description = 'smk - ตั้งหนี้';
            Caption = 'ตั้งหนี้';
        }
    }
    keys
    {
        key(PrimaryKey; "SMK Order No.", "SMK Order Type")
        {
            Clustered = true;
        }
    }
}