table 60026 "SMK Cue"
{
    fields
    {
        field(1; "SMK Entry No."; Integer)
        {
            DataClassification = SystemMetadata;
        }
        field(2; "SMK No. Front Rcpt. Not Lock"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("Sales Header" where("Document Type" = const("SMK Front Receipt"), "SMK Front Receipt Lock" = const(false)));
            Editable = false;
        }
        field(3; "SMK No. Front Rcpt. Lock"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("Sales Header" where("Document Type" = const("SMK Front Receipt"), "SMK Front Receipt Lock" = const(true), "SMK Front Send to RV" = const(false)));
            Editable = false;
        }
        field(4; "SMK No. Front Rcpt. RV"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = count("Sales Header" where("Document Type" = const("SMK Front Receipt"), "SMK Front Receipt Lock" = const(true), "SMK Front Send to RV" = const(true)));
            Editable = false;
        }

    }
}