table 60004 "SMK Temp. Credit Limit Entry"
{
    fields
    {

        field(70000; "SMK Entry No."; Integer)
        {
            DataClassification = ToBeClassified;
        }
        field(70001; "SMK Customer No."; Code[20])
        {
            DataClassification = ToBeClassified;
        }
        field(70002; "SMK Product Group No."; Code[20])
        {
            DataClassification = ToBeClassified;
            TableRelation = "Dimension Value"."Code" where("Dimension Code" = const('PRODUCTGROUP'));
        }
        field(70003; "SMK Product No."; Code[20])
        {
            DataClassification = ToBeClassified;
            TableRelation = "Dimension Value"."Code" where("Dimension Code" = const('PRODUCT'), Code = field("SMK Product No."));
        }
        field(70004; "SMK Credit Amount"; Decimal)
        {
            DataClassification = ToBeClassified;
        }
        field(70005; "SMK Expire Date"; Date)
        {
            DataClassification = ToBeClassified;
        }
        field(70006; "SMK Keyin Date"; DateTime)
        {
            DataClassification = SystemMetadata;
            Editable = false;
        }
        field(70007; "SMK Keyin User"; Code[20])
        {
            DataClassification = SystemMetadata;
            Editable = false;
        }
        field(70008; "SMK Cancelled"; Boolean)
        {
            DataClassification = SystemMetadata;
            Editable = false;
            trigger OnValidate()
            begin
                "SMK Cancelled Date" := CurrentDateTime;
                if LoginInfor.GET(SessionId(), ServiceInstanceId()) then
                    "SMK Cancelled User" := LoginInfor."BW USER Name";
            end;
        }
        field(70009; "SMK Cancelled Date"; DateTime)
        {
            DataClassification = SystemMetadata;
            Editable = false;
        }
        field(70010; "SMK Cancelled User"; Code[20])
        {
            DataClassification = SystemMetadata;
            Editable = false;
        }
    }
    keys
    {
        key(PrimaryKey; "SMK Entry No.")
        {
            Clustered = true;
        }
    }

    trigger OnInsert()
    begin
        "SMK Entry No." := GetLastEntryNo() + 1;
        "SMK Keyin Date" := CurrentDateTime;
        if LoginInfor.GET(SessionId(), ServiceInstanceId()) then
            "SMK Keyin User" := LoginInfor."BW USER Name";
    end;

    /// <summary> 
    /// Description for GetLastEntryNo.
    /// </summary>
    /// <returns>Return variable "Integer".</returns>
    procedure GetLastEntryNo(): Integer
    var
        LTempCreditLimit: Record "SMK Temp. Credit Limit Entry";
    begin
        LTempCreditLimit.reset;
        if LTempCreditLimit.FindLast() then
            exit(LTempCreditLimit."SMK Entry No.")
        else
            exit(0);
    end;

    var
        LoginInfor: Record "BW LogInformation";
}