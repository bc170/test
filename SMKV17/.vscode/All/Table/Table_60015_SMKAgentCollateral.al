table 60015 "SMK Agent Collateral"
{
    DataClassification = CustomerContent;

    fields
    {
        field(1; "Customer No."; Code[20])
        {
            DataClassification = CustomerContent;
            TableRelation = Customer."No.";
        }
        field(2; "Line No."; Integer)
        {
            DataClassification = SystemMetadata;
        }
        field(3; "Collateral Description"; Text[250])
        {
            DataClassification = CustomerContent;
        }
        field(4; "Collateral Amount"; Decimal)
        {
            DataClassification = CustomerContent;
        }
        field(5; "Collateral Credit"; Decimal)
        {
            DataClassification = CustomerContent;
        }
        field(6; "Keyin User"; Code[5])
        {
            DataClassification = CustomerContent;
        }
        field(7; "Approve Status"; option)
        {
            OptionMembers = " ","Open","Pending Approve","Released";
            DataClassification = CustomerContent;
        }
        field(8; "Approve Date"; Date)
        {
            DataClassification = CustomerContent;
        }
    }

    keys
    {
        key(Key1; "Customer No.", "Line No.")
        {
            Clustered = true;
        }
    }
}