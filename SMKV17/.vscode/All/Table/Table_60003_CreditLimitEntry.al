table 60003 "SMK Credit Limit Entry"
{
    fields
    {
        field(70000; "SMK Level"; Integer)
        {
            DataClassification = ToBeClassified;
        }
        field(70001; "SMK Customer No."; Code[20])
        {
            DataClassification = ToBeClassified;
        }
        field(70002; "SMK Product Group No."; Code[20])
        {
            DataClassification = ToBeClassified;
            TableRelation = "Dimension Value"."Code" where("Dimension Code" = const('PRODUCTGROUP'));
        }
        field(70003; "SMK Product No."; Code[20])
        {
            DataClassification = ToBeClassified;
            TableRelation = "Dimension Value"."Code" where("Dimension Code" = const('PRODUCT'), Code = field("SMK Product No."));
        }
        field(70004; "SMK Credit Amount"; Decimal)
        {
            DataClassification = ToBeClassified;
        }
        field(70005; "SMK Hold"; Boolean)
        {
            DataClassification = ToBeClassified;
        }
    }
    keys
    {
        key(PrimaryKey; "SMK Customer No.", "SMK Product Group No.", "SMK Product No.")
        {
            Clustered = true;
        }
    }

    /// <summary> 
    /// Description for GetSummaryTempCreditLimit.
    /// </summary>
    /// <param name="PCurrDate">Parameter of type Date.</param>
    /// <returns>Return variable "Decimal".</returns>
    procedure "SMK GetSummaryTempCreditLimit"(PCurrDate: Date): Decimal
    var
        TempCreditLimit: Record "SMK Temp. Credit Limit Entry";
    begin
        TempCreditLimit.reset;
        TempCreditLimit.SetRange("SMK Customer No.", "SMK Customer No.");
        TempCreditLimit.SetRange("SMK Product Group No.", "SMK Product Group No.");
        TempCreditLimit.SetRange("SMK Product No.", "SMK Product No.");
        TempCreditLimit.SetRange("SMK Cancelled", false);
        TempCreditLimit.SetFilter("SMK Expire Date", '>%1', PCurrDate);
        if TempCreditLimit.FindSet() then begin
            TempCreditLimit.CalcSums("SMK Credit Amount");
            exit(TempCreditLimit."SMK Credit Amount");
        end else
            exit(0);
    end;

    /// <summary> 
    /// Description for BW GetSummaryOnCustInsurance.
    /// </summary>
    /// <returns>Return variable "Decimal".</returns>
    procedure "BW GetSummaryOnCustInsurance"(): Decimal
    var
    //LRecInsurance: Record "BW Insurance";
    begin
    end;
}