table 60014 "SMK Agent Card"
{
    DataClassification = CustomerContent;

    fields
    {
        field(1; "Customer No."; Code[20])
        {
            DataClassification = CustomerContent;
            TableRelation = Customer."No.";
        }
        field(2; "Card No."; Code[5])
        {
            DataClassification = CustomerContent;
        }
        field(3; "Credit Amount"; Decimal)
        {
            DataClassification = CustomerContent;
        }
        field(4; "Unlimit"; Boolean)
        {
            DataClassification = CustomerContent;
        }
        field(5; "Used Credit"; decimal)
        {
            DataClassification = CustomerContent;
        }
        field(6; "Keyin User"; COde[50])
        {
            DataClassification = CustomerContent;
        }
        field(7; "Approve Status"; option)
        {
            OptionMembers = " ","Open","Pending Approve","Released";
            DataClassification = CustomerContent;
        }
        field(8; "Approve Date"; Date)
        {
            DataClassification = CustomerContent;
        }
    }

    keys
    {
        key(Key1; "Customer No.", "Card No.")
        {
            Clustered = true;
        }
    }
}