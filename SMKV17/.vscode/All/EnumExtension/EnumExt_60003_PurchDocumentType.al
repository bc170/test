enumextension 60003 "SMK Purch. Document Type" extends "Purchase Document Type"
{
    value(60000; "SMK Front Bank Deposit")
    {
        Caption = 'Front Bank Deposit';
    }
}