enumextension 60004 "SMK Purch. Comment Doc. Type" extends "Purchase Comment Document Type"
{
    value(70000; "SMK Front Bank Deposit")
    {
        Caption = 'Front Bank Deposit';
    }
}