enumextension 60000 "SMK ApproveDocumentType" extends "Approval Document Type"
{
    value(70000; "SMK Discount Promotion Setup") { Caption = 'Discount Promotion'; }
    value(70001; "SMK Period Promotion Setup") { Caption = 'Period Promotion'; }
    value(70002; "SMK Incentive Setup") { Caption = 'Incentive Setup'; }
    value(70003; "SMK Commission Setup") { Caption = 'Commission Setup'; }
}