enumextension 60002 "BW SalesCommentDocType" extends "Sales Comment Document Type"
{
    value(60000; "SMK Front Receipt")
    {
        Caption = 'Front Receipt';
    }
    value(60001; "SMK Remittance Note")
    {
        Caption = 'ใบนำส่งเงิน';
    }
}