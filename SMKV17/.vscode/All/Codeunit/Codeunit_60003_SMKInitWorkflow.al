codeunit 60003 "SMK Init Workflow"
{
    trigger OnRun()
    begin

    end;

    //Discount Promotion Setup >>>>
    [IntegrationEvent(false, false)]
    PROCEDURE OnSendDiscPromotionforApproval(var DiscPromoSetup: Record "SMK Promo. Disc. Setup");
    begin
    end;

    [IntegrationEvent(false, false)]
    PROCEDURE OnCancelDiscPromotionforApproval(var DiscPromoSetup: Record "SMK Promo. Disc. Setup");
    begin
    end;

    procedure IsDiscPromotionEnabled(var DiscPromoSetup: Record "SMK Promo. Disc. Setup"): Boolean
    var
        WFMngt: Codeunit "Workflow Management";
        WFCode: Codeunit "SMK Workflow";
    begin
        exit(WFMngt.CanExecuteWorkflow(DiscPromoSetup, WFCode.RunWorkflowOnSendDiscPromotionApprovalCode()));
    end;

    procedure CheckWorkflowDiscPromotionEnabled(): Boolean
    var
        DiscPromoSetup: Record "SMK Promo. Disc. Setup";
        NoWorkflowEnb: Label 'No workflow Enabled for this Record type';
    begin
        if not IsDiscPromotionEnabled(DiscPromoSetup) then
            Error(NoWorkflowEnb);
    end;
    //Discount Promotion Setup <<<<
}