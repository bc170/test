codeunit 60002 "SMK Workflow"
{
    trigger OnRun()
    begin

    end;

    var
        WFMngt: Codeunit "Workflow Management";
        AppMgmt: Codeunit "Approvals Mgmt.";
        WorkflowEventHandling: Codeunit "Workflow Event Handling";
        WorkflowResponseHandling: Codeunit "Workflow Response Handling";
        SendReqDiscPromotion: Label 'Approval Request for Discount Promotion is requested.';
        CancelDiscPromotion: Label 'Apporval of a Discount Promotion is canceled.';
        DiscPromotionTypeCondi: Label '<?xml version = "1.0" encoding="utf-8" standalone="yes"?><ReportParameters><DataItems><DataItem name="SMK Disc. Promo. Setup">%1</DataItem></DataItems></ReportParameters>';

    //Setup New Event
    //Discount Promotion Setup >>>>
    procedure RunWorkflowOnSendDiscPromotionApprovalCode(): Code[128]
    begin
        exit(UpperCase('RunWorkflowOnSendDiscPromotionApproval'))
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"SMK Init Workflow", 'OnSendDiscPromotionforApproval', '', false, false)]
    procedure RunWorkflowOnSendDiscPromotionApproval(var DiscPromoSetup: Record "SMK Promo. Disc. Setup")
    begin
        WFMngt.HandleEvent(RunWorkflowOnSendDiscPromotionApprovalCode(), DiscPromoSetup);
    end;

    procedure RunWorkflowOnCancelDiscPromotionApprovalCode(): Code[128]
    begin
        exit(UpperCase('RunWorkflowOnCancelDiscPromotionApproval'))
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"SMK Init Workflow", 'OnCancelDiscPromotionforApproval', '', false, false)]
    procedure OnCancelDiscPromotionforApproval(var DiscPromoSetup: Record "SMK Promo. Disc. Setup")
    begin
        WFMngt.HandleEvent(RunWorkflowOnCancelDiscPromotionApprovalCode(), DiscPromoSetup);
    end;

    local procedure "SMK DetailWorkflowDiscPromoSetup"(var workflow: Record Workflow)
    var
        WorkflowSetpArgument: Record 1523;
        blankDateFormula: DateFormula;
        workflowResponseHanding: Codeunit 1521;
        WorkflowSetup: Codeunit "Workflow Setup";
        DiscPromoSetup: Record "SMK Promo. Disc. Setup";
    begin
        WorkflowSetup.PopulateWorkflowStepArgument(WorkflowSetpArgument,
        WorkflowSetpArgument."Approver Type"::Approver, WorkflowSetpArgument."Approver Limit Type"::"Direct Approver",
        0, '', blankDateFormula, TRUE);

        WorkflowSetup.InsertDocApprovalWorkflowSteps(
          workflow,
          "SMK BuildCondiftion"(DiscPromoSetup."SMK Approve Status"::Open, Database::"SMK Promo. Disc. Setup"),
          RunWorkflowOnSendDiscPromotionApprovalCode(),
          "SMK BuildCondiftion"(DiscPromoSetup."SMK Approve Status"::"Pending Approval", Database::"SMK Promo. Disc. Setup"),
          RunWorkflowOnCancelDiscPromotionApprovalCode(),
          WorkflowSetpArgument,
          TRUE
      );
    end;

    local procedure "SMK InsertWorkflowTemplateDiscPromoSetup"()
    var
        Workflow: Record 1501;
        workflowSetup: Codeunit "Workflow Setup";
    begin
        workflowSetup.InsertWorkflowTemplate(Workflow, 'DISCOUNTPROMOTION', 'Discount Promotion Setup', 'DISCOUNTPROMOTION');
        "SMK DetailWorkflowDiscPromoSetup"(Workflow);
        workflowSetup.MarkWorkflowAsTemplate(Workflow);
    end;

    //Discount Promotion Setup <<<<
    //Other >>>
    //Other <<<

    //Setup New Event <<<<

    //Event All >>>>
    procedure RunWorkflowOnApproveSMKApprovalCode(): Code[128]
    begin
        exit(UpperCase('RunWorkflowOnApproveSMKApproval'))
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Approvals Mgmt.", 'OnApproveApprovalRequest', '', false, false)]
    procedure RunWorkflowOnApproveSMKApproval(var ApprovalEntry: Record "Approval Entry")
    begin
        WFMngt.HandleEventOnKnownWorkflowInstance(RunWorkflowOnApproveSMKApprovalCode(), ApprovalEntry, ApprovalEntry."Workflow Step Instance ID");
    end;

    procedure RunWorkflowOnRejectSMKLineApprovalCode(): Code[128]
    begin
        exit(UpperCase('RunWorkflowOnRejectSMKLineApproval'))
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Approvals Mgmt.", 'OnRejectApprovalRequest', '', false, false)]
    procedure RunWorkflowOnRejectSMKLineApproval(var ApprovalEntry: Record "Approval Entry")
    begin
        WFMngt.HandleEventOnKnownWorkflowInstance(RunWorkflowOnRejectSMKLineApprovalCode(), ApprovalEntry, ApprovalEntry."Workflow Step Instance ID");
    end;

    procedure RunWorkflowOnDelegateSMKApprovalCode(): Code[128]
    begin
        exit(UpperCase('RunWorkflowOnDelegateSMKApproval'))
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Approvals Mgmt.", 'OnDelegateApprovalRequest', '', false, false)]
    procedure RunWorkflowOnDelegateSMKApproval(var ApprovalEntry: Record "Approval Entry")
    begin
        WFMngt.HandleEventOnKnownWorkflowInstance(RunWorkflowOnDelegateSMKApprovalCode(), ApprovalEntry, ApprovalEntry."Workflow Step Instance ID");
    end;



    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Workflow Event Handling", 'OnAddWorkflowEventsToLibrary', '', false, false)]
    procedure AddSMKEventToLibrary()
    var
        WorkFlowEvent: Record "Workflow Event";
    begin
        //Add for Discount Promotion >>>>
        WorkFlowEvent.reset;
        WorkFlowEvent.SetRange(Description, SendReqDiscPromotion);
        if WorkFlowEvent.FindFirst() then
            WorkFlowEvent.Delete(true);
        WorkFlowEvent.reset;
        WorkFlowEvent.SetRange(Description, CancelDiscPromotion);
        if WorkFlowEvent.FindFirst() then
            WorkFlowEvent.Delete(true);

        WorkflowEventHandling.AddEventToLibrary(RunWorkflowOnSendDiscPromotionApprovalCode(), Database::"SMK Promo. Disc. Setup", SendReqDiscPromotion, 0, false);
        WorkflowEventHandling.AddEventToLibrary(RunWorkflowOnCancelDiscPromotionApprovalCode(), Database::"SMK Promo. Disc. Setup", CancelDiscPromotion, 0, false);
        //Add for Discount Promotion <<<<
        //Add Other >>>>
        //Add Other <<<<
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Approvals Mgmt.", 'OnPopulateApprovalEntryArgument', '', false, false)]

    local procedure OnPopulateApprovalEntryArgument(var RecRef: RecordRef; var ApprovalEntryArgument: Record "Approval Entry"; WorkflowStepInstance: Record "Workflow Step Instance");
    var
        DiscPromoSetup: Record "SMK Promo. Disc. Setup";
    begin
        case RecRef.Number OF
            DATABASE::"SMK Promo. Disc. Setup":
                // Discount Promotion Setup >>>>
                begin
                    RecRef.SetTable(DiscPromoSetup);
                    ApprovalEntryArgument."Document Type" := ApprovalEntryArgument."Document Type"::"SMK Discount Promotion Setup";
                    ApprovalEntryArgument."Document No." := DiscPromoSetup."SMK Promotion Code";
                end;
        //Discount Promotion Setup <<<<
        //Other >>>>
        //Other <<<<
        end;
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Page Management", 'OnAfterGetPageID', '', false, false)]
    local procedure "SMK OnAfterGetPageID"(RecordRef: RecordRef; var PageID: Integer)
    begin
        if (PageID = 0) and (RecordRef.Number = Database::"SMK Promo. Disc. Setup") then
            PageID := Page::"SMK Disc. Promo. Setups";
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Notification Management", 'OnGetDocumentTypeAndNumber', '', false, false)]
    local procedure "SMK OnGetDocumentTypeAndNumber"(var RecRef: RecordRef; var IsHandled: Boolean; var DocumentNo: Text; var DocumentType: Text);
    var
        FieldRef: FieldRef;
    begin
        //Discount Promotion Setup >>>>
        if RecRef.Number = DATABASE::"SMK Promo. Disc. Setup" then begin
            DocumentType := RecRef.Caption;
            FieldRef := RecRef.Field(1);
            DocumentNo := Format(FieldRef.Value);
            IsHandled := true;
        end;
        //Discount Promotion Setup <<<<
        //Other >>>>
        //Other <<<<
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Approvals Mgmt.", 'OnSetStatusToPendingApproval', '', false, false)]
    local procedure "SMK OnSetStatusToPendingApproval"(RecRef: RecordRef; var Variant: Variant; var IsHandled: Boolean)
    var
        DiscPromoSetup: Record "SMK Promo. Disc. Setup";
    begin
        case RecRef.Number of
            //Discount Promotion Setup >>>>
            Database::"SMK Promo. Disc. Setup":
                begin
                    RecRef.SetTable(DiscPromoSetup);
                    DiscPromoSetup.Validate("SMK Approve Status", DiscPromoSetup."SMK Approve Status"::"Pending Approval");
                    DiscPromoSetup.Modify();
                    IsHandled := true;
                end;
        //Discount Promotion Setup <<<<
        //Other >>>>
        //Other <<<<
        end;
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Workflow Response Handling", 'OnReleaseDocument', '', false, false)]
    local procedure "SMK OnReleaseDocument"(RecRef: RecordRef; var Handled: Boolean);
    var
        DiscPromoSetup: Record "SMK Promo. Disc. Setup";
    begin
        case RecRef.Number of
            //Discount Promotion Setup >>>>
            Database::"SMK Promo. Disc. Setup":
                begin
                    RecRef.SetTable(DiscPromoSetup);
                    DiscPromoSetup.Validate("SMK Approve Status", DiscPromoSetup."SMK Approve Status"::Released);
                    DiscPromoSetup.Modify();
                    Handled := true;
                end;
        //Discount Promotion Setup <<<<
        //Other

        //Other
        end;
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Workflow Response Handling", 'OnOpenDocument', '', false, false)]
    local procedure OnOpenDocument(RecRef: RecordRef; var Handled: Boolean);
    var
        DiscPromoSetup: Record "SMK Promo. Disc. Setup";
    begin
        case RecRef.Number of
            //Discount Promotion Setup >>>>
            Database::"SMK Promo. Disc. Setup":
                begin
                    RecRef.SetTable(DiscPromoSetup);
                    DiscPromoSetup.Validate("SMK Approve Status", DiscPromoSetup."SMK Approve Status"::Open);
                    DiscPromoSetup.Modify();
                    Handled := true;
                end;
        //Discount Promotion Setup <<<<
        //Other >>>>
        //Other <<<<
        end;
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Workflow Setup", 'OnAddWorkflowCategoriesToLibrary', '', false, false)]
    local procedure OnAddWorkflowCategoriesToLibrary();
    var
        workflowSetup: Codeunit "Workflow Setup";
    begin
        //Discount Promotion Setup >>>>
        workflowSetup.InsertWorkflowCategory('DISCOUNTPROMOTION', 'Discount Promotion Setup');
        //Discount Promotion Setup <<<<
        //Other >>>>
        //Other <<<<
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Workflow Setup", 'OnAfterInsertApprovalsTableRelations', '', false, false)]
    local procedure "SMK OnAfterInsertApprovalsTableRelations"()
    var
        ApprovalEntry: Record "Approval Entry";
        workflowSetup: Codeunit "Workflow Setup";
        workflowwebhook: Record "Workflow Webhook Entry";
    begin
        //Discount Promotion Setup >>>>
        workflowSetup.InsertTableRelation(Database::"SMK Promo. Disc. Setup", 0, Database::"Approval Entry", ApprovalEntry.FieldNo("Record ID to Approve"));
        workflowSetup.InsertTableRelation(Database::"SMK Promo. Disc. Setup", 11, Database::"Workflow Webhook Entry", workflowwebhook.FieldNo("Data ID"));
        //Discount Promotion Setup <<<<
    end;

    local procedure "SMK BuildCondiftion"(Status: Enum "Sales Document Status"; TableID: Integer): Text
    var
        workflowSetup: Codeunit "Workflow Setup";
        DiscPromoSetup: Record "SMK Promo. Disc. Setup";
    begin
        //Discount Promotion Setup >>>>
        if TableID = Database::"SMK Promo. Disc. Setup" then begin
            DiscPromoSetup.SetRange("SMK Approve Status", Status);
            exit(StrSubstNo(DiscPromotionTypeCondi, workflowSetup.Encode(DiscPromoSetup.GetView(false))));
        end;
        //Discount Promotion Setup <<<<
        //Other >>>>
        //Other <<<<
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Workflow Setup", 'OnInsertWorkflowTemplates', '', false, false)]
    local procedure "SMK OnInsertWorkflowTemplates"()
    begin
        //Discount Promotion Setup >>>>
        "SMK InsertWorkflowTemplateDiscPromoSetup"();
        //Discount Promotion Setup <<<<
        //Other >>>>
        //Other <<<<
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Workflow Event Handling", 'OnAddWorkflowEventPredecessorsToLibrary', '', false, false)]
    local procedure "LS OnAddWorkflowEventPredecessorsToLibrary"(EventFunctionName: Code[128]);
    var
        WorkflowEventHadning: Codeunit "Workflow Event Handling";
    begin
        case EventFunctionName of
            //Discount Promotion Setup >>>>
            RunWorkflowOnCancelDiscPromotionApprovalCode():
                begin
                    WorkflowEventHadning.AddEventPredecessor(RunWorkflowOnCancelDiscPromotionApprovalCode(), RunWorkflowOnSendDiscPromotionApprovalCode());
                end;
            //Discount Promotion Setup <<<<
            //Other >>>
            //Other <<<
            WorkflowEventHadning.RunWorkflowOnApproveApprovalRequestCode():
                begin
                    //Discount Promotion Setup >>>>
                    WorkflowEventHadning.AddEventPredecessor(WorkflowEventHadning.RunWorkflowOnApproveApprovalRequestCode(), RunWorkflowOnSendDiscPromotionApprovalCode());
                    //Discount Promotion Setup <<<<
                    //Other >>>>
                    //Other <<<<
                end;

        end;
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Workflow Response Handling", 'OnAddWorkflowResponsePredecessorsToLibrary', '', false, false)]
    local procedure "LS OnAddWorkflowResponsePredecessorsToLibrary"(ResponseFunctionName: Code[128]);
    var
        WorkflowResponseHanding: Codeunit 1521;
    begin
        case ResponseFunctionName of

            WorkflowResponseHanding.SetStatusToPendingApprovalCode():
                begin
                    //Discount Promotion Setup >>>>
                    WorkflowResponseHanding.AddResponsePredecessor(WorkflowResponseHanding.SetStatusToPendingApprovalCode(),
                    RunWorkflowOnSendDiscPromotionApprovalCode());
                    //Discount Promotion Setup <<<<
                    //Other >>>>
                    //Other <<<<
                end;
            WorkflowResponseHanding.SendApprovalRequestForApprovalCode():
                begin
                    //Discount Promotion Setup >>>>
                    WorkflowResponseHanding.AddResponsePredecessor(WorkflowResponseHanding.SendApprovalRequestForApprovalCode(),
                    RunWorkflowOnSendDiscPromotionApprovalCode());
                    //Discount Promotion Setup <<<<
                    //Other >>>>
                    //Other <<<<
                end;
            WorkflowResponseHanding.RejectAllApprovalRequestsCode():
                begin
                    WorkflowResponseHanding.AddResponsePredecessor(WorkflowResponseHanding.RejectAllApprovalRequestsCode(),
                    RunWorkflowOnRejectSMKLineApprovalCode());
                end;
            WorkflowResponseHanding.CancelAllApprovalRequestsCode():
                begin
                    //Discount Promotion Setup >>>>
                    WorkflowResponseHanding.AddResponsePredecessor(WorkflowResponseHanding.CancelAllApprovalRequestsCode(),
                    RunWorkflowOnCancelDiscPromotionApprovalCode());
                    //Discount Promotion Setup <<<<
                    //Other >>>>
                    //Other <<<<
                end;
            WorkflowResponseHanding.OpenDocumentCode():
                begin
                    //Discount Promotion Setup >>>>
                    WorkflowResponseHanding.AddResponsePredecessor(WorkflowResponseHanding.OpenDocumentCode(),
                    RunWorkflowOnCancelDiscPromotionApprovalCode());
                    //Discount Promotion Setup <<<<
                    //Other >>>>
                    //Other <<<<
                end;
        end;

    end;



    //Event All <<<<



}