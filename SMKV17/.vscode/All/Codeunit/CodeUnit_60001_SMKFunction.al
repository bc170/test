codeunit 60001 "SMK Function"
{
    EventSubscriberInstance = StaticAutomatic;
    /// <summary> 
    /// Description for BWK RunPromotionDiscountPreviewPage.
    /// </summary>
    /// <param name="PPromotion">Parameter of type Record "SMK Promo. Disc. Setup".</param>
    /// <param name="PTmpPromoDiscEntry">Parameter of type Record temporary.</param>
    procedure "SMK RunPromotionDiscountPreviewPage"(PPromotion: Record "SMK Promo. Disc. Setup"; var PTmpPromoDiscEntry: Record "SMK Promo. Disc. Entry" temporary)
    var
        LProFactor: Record "SMK Promo. Disc. Factor";
        LOrders: Record "SMK Orders";
        LRecordRef: RecordRef;
        LFieldRef: FieldRef;
        LTmpPromoDiscEntry: Record "SMK Promo. Disc. Entry" temporary;
        LCountEntry: Integer;
        LPromoDiscEntryPreview: Page "SMK Promotion Entry Review";
        LWindow: Dialog;
    begin

        LWindow.Open('Process....\###1###\Total Record : ###2###\Current Record : ###3###');
        LOrders.Reset;
        LProFactor.reset;
        LProFactor.SetRange("SMK Promotion Code", PPromotion."SMK Promotion Code");
        Clear(LRecordRef);
        LRecordRef.Open(Database::"SMK Orders");
        LWindow.Update(1, 'Read Filter');
        if LProFactor.FindSet() then begin
            repeat
                LRecordRef.Field(LProFactor."SMK Factor Field").SetFilter(LProFactor."SMK Filter Expression");
            until LProFactor.Next() = 0;
        end;

        LOrders.reset;
        LRecordRef.SetTable(LOrders);

        //Message('1 : %1\======\2 : %2\======\%3', LRecordRef.GetFilters, LPolicy.GetFilters, LPolicy.Count);
        LWindow.Update(1, 'Calc. Data');
        LWindow.Update(2, LOrders.Count);
        LCountEntry := 0;
        if LOrders.FindSet() then begin
            repeat
                LWindow.Update(3, Abs(LCountEntry));
                LCountEntry -= 1;
                PTmpPromoDiscEntry.Init();
                PTmpPromoDiscEntry."SMK Entry No." := LCountEntry;
                PTmpPromoDiscEntry."SMK Policy Code" := LOrders."SMK Policy No.";
                PTmpPromoDiscEntry."SMK Promotion Code" := PPromotion."SMK Promotion Code";
                PTmpPromoDiscEntry."SMK Discount Amount" := ((LOrders."SMK Premium Net" * PPromotion."SMK Discount %") / 100) + PPromotion."SMK Discount Amount";
                PTmpPromoDiscEntry.Insert();
            until LOrders.Next() = 0;
        end;
        LWindow.Close();
    end;

    //[EventSubscriber(ObjectType::)]
}