page 60025 "SMK Agent Collateral"
{
    PageType = List;
    caption = 'Agent Collateral';
    SourceTable = "SMK Agent Collateral";
    SourceTableTemporary = false;
    DeleteAllowed = true;
    ModifyAllowed = true;
    InsertAllowed = true;
    DelayedInsert = true;
    MultipleNewLines = true;
    AutoSplitKey = true;

    layout
    {
        area(Content)
        {
            repeater("Lines")
            {
                field("Customer No."; Rec."Customer No.")
                {
                    Visible = false;
                    Editable = false;
                }
                field("Line No."; Rec."Line No.")
                {
                    Editable = false;
                    Visible = false;
                }
                field("Collateral Description"; Rec."Collateral Description")
                {
                    ApplicationArea = All;
                    Caption = 'Asset';
                }
                field("Collateral Amount"; Rec."Collateral Amount")
                {
                    ApplicationArea = All;
                    caption = 'Fund';
                }
                field("Collateral Credit"; Rec."Collateral Credit")
                {
                    ApplicationArea = All;
                    Caption = 'Credit';
                }
                field("Approve Status"; Rec."Approve Status")
                {
                    ApplicationArea = All;
                }
                field("Approve Date"; Rec."Approve Date")
                {
                    ApplicationArea = All;
                }
                field("Keyin User"; Rec."Keyin User")
                {
                    ApplicationArea = All;

                }
            }
        }
    }

    actions
    {
        area(Processing)
        {

            action(SendApprove)
            {
                ApplicationArea = all;
                Caption = 'Send Approve';
                Image = SendApprovalRequest;

                trigger OnAction()
                begin
                    llApprove := true;
                end;
            }
            action(CancelApprove)
            {
                ApplicationArea = all;
                Caption = 'Cancel Approve';
                Image = CancelApprovalRequest;

                trigger OnAction()
                begin
                    llApprove := false;
                end;
            }

        }
    }

    var
        llApprove: Boolean;
        CustNo: code[20];

    procedure SetCust(PCustNo: code[20])
    begin
        CustNo := PCustNo;
    end;
}