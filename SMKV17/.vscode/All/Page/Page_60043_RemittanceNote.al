page 60043 "SMK Remittance Note"
{
    SourceTable = "Sales Header";
    Caption = 'ใบนำส่ง';
    PageType = Card;
    SourceTableView = where("Document Type" = const("SMK Remittance Note"));
    layout
    {
        area(Content)
        {
            group(General)
            {
                Caption = 'ทั่วไป';
                field("No."; Rec."No.")
                {
                    Caption = 'เลขที่เอกสาร';
                    ApplicationArea = All;
                }
                field("SMK Receipt From"; Rec."SMK Receipt From")
                {
                    ApplicationArea = All;
                }
                field("SMK Agent No."; Rec."SMK Agent No.")
                {
                    ApplicationArea = All;
                }
                field("SMK Department Code"; Rec."SMK Department Code")
                {
                    ApplicationArea = All;
                }
                field("SMK Department Name"; Rec."SMK Department Name")
                {
                    ApplicationArea = All;
                }
                field("SMK Branch Code"; Rec."SMK Branch Code")
                {
                    ApplicationArea = All;
                }
                field("SMK Branch Name"; Rec."SMK Branch Name")
                {
                    ApplicationArea = All;
                }
            }
        }
    }
}