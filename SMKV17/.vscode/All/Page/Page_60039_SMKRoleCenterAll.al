
page 60039 "SMK Main Role Center"
{
    PageType = RoleCenter;
    Caption = 'SMK Role Main';
    Editable = false;
    layout
    {
        area(RoleCenter)
        {
            part(SMKFrontCue; "SMK Front Activity Cues")
            {
                ApplicationArea = All;
            }
        }
    }
    actions
    {
        area(Sections)
        {
            group(SMKActionGroupFront)
            {
                Caption = 'หน้าร้านสาขา';
                action("SMKActionFrontReceipt")
                {
                    ApplicationArea = All;
                    Caption = 'ใบรับเงิน';
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    Image = Receipt;
                    RunObject = page "SMK Front Receipt List";
                }
                action("SMKActionFrontBankDeposit")
                {
                    ApplicationArea = All;
                    Caption = 'ใบนำส่งเงินสด';
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    Image = BankAccount;
                    RunObject = page "SMK Front Bank Deposit List";
                }

                action(SMKTestRemittance)
                {
                    ApplicationArea = All;
                    Caption = 'ใบนำส่งเงิน';
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    Image = Receipt;
                    RunObject = page "SMK Remittance Notes";
                }

            }
            group(SMKActionGroupSetup)
            {
                Caption = 'ตั้งค่า';
                action("SMKActionDiscountPromotionSetup")
                {
                    ApplicationArea = All;
                    Caption = 'ตั้งค่าโปรโมชั่น Customer';
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    Image = Setup;
                    RunObject = page "SMK Disc. Promo. Setups";
                }
                action("SMKActionPeriodPromotionSetup")
                {
                    ApplicationArea = All;
                    Caption = 'ตั้งค่าโปรโมชั่น Agent';
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    Image = Setup;
                    RunObject = page "SMK Period Promo. Setup";
                }
                action("SMKActionIncentiveSetup")
                {
                    ApplicationArea = All;
                    Caption = 'ตั้งค่า Incentive';
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    Image = Setup;
                    RunObject = page "SMK Incentive Policy List";
                }
                action("SMKActionCommissionSetup")
                {
                    ApplicationArea = All;
                    Caption = 'ตั้งค่า Commission';
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    Image = Setup;
                    RunObject = page "SMK Commission Policy Setup";
                }

            }
            group(SMKActionGroupList)
            {
                Caption = 'รายการ';
                action("SMKActionOrders")
                {
                    ApplicationArea = All;
                    Caption = 'รายการใบแจ้งงาน';
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    Image = List;
                    RunObject = page "SMK Order List";
                }
                action("SMKActionBilling")
                {
                    ApplicationArea = All;
                    Caption = 'รายการใบวางบิล';
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    Image = List;
                    RunObject = page "SMK Billing List";
                }
            }
            group(SMKActionGroupMaster)
            {
                Caption = 'Master';
                action("SMKAgent")
                {
                    ApplicationArea = All;
                    Caption = 'Agent';
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    Image = List;
                    RunObject = page "Customer List";
                }
            }
        }
    }
}
