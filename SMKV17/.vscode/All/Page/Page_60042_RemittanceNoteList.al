page 60042 "SMK Remittance Notes"
{
    SourceTable = "Sales Header";
    Caption = 'ใบนำส่ง';
    SourceTableView = where("Document Type" = const("SMK Remittance Note"));
    PageType = List;
    CardPageId = "SMK Remittance Note";
    layout
    {
        area(Content)
        {
            repeater(content1)
            {
                field("No."; Rec."No.")
                {
                    ApplicationArea = All;
                }
                field(Amount; Rec."Amount")
                {
                    ApplicationArea = All;
                }
                field("SMK Receipt From"; Rec."SMK Receipt From")
                {
                    ApplicationArea = All;
                }
                field("SMK Department Code"; Rec."SMK Department Code")
                {
                    ApplicationArea = All;
                }
                field("SMK Branch Code"; Rec."SMK Branch Code")
                {
                    ApplicationArea = All;
                }
                field("SMK Pay Amount"; Rec."SMK Pay Amount")
                {
                    ApplicationArea = All;
                }

            }
        }
    }
}