page 60004 "SMK Order List"
{

    ApplicationArea = All;
    Caption = 'ใบแจ้งงาน';
    PageType = List;
    SourceTable = "SMK Orders";
    UsageCategory = Lists;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field("SMK Order No."; Rec."SMK Order No.")
                {
                    ApplicationArea = All;
                    Caption = 'เลขที่รับแจ้ง';
                }
                field("SMK Order Type"; Rec."SMK Order Type")
                {
                    ApplicationArea = All;
                    Caption = 'ประเภทงาน';
                }
                field("SMK Policy No."; Rec."SMK Policy No.")
                {
                    ApplicationArea = All;
                    Caption = 'เลขกรมธรรม์';
                }
                field("SMK Policy Date"; Rec."SMK Policy Date")
                {
                    ApplicationArea = All;
                    Caption = 'วันที่ผลิต กธ.';
                }
                field("SMK Policy Status"; Rec."SMK Policy Status")
                {
                    ApplicationArea = All;
                    Caption = 'สถานะ กธ.';
                }
                field("SMK Account Period Date"; Rec."SMK Account Period Date")
                {
                    ApplicationArea = All;
                    Caption = 'วันรอบบัญชี';
                }
                field("SMK Agent No."; Rec."SMK Agent No.")
                {
                    ApplicationArea = All;
                    Caption = 'รหัสตัวแทน';
                }
                field("SMK Branch"; Rec."SMK Branch")
                {
                    ApplicationArea = All;
                    Caption = 'สาขากรมธรรม์';
                }
                field("SMK Broker No."; Rec."SMK Broker No.")
                {
                    ApplicationArea = All;
                    Caption = 'รหัสโบรกเกอร์';
                }
                field("SMK Campaign"; Rec."SMK Campaign")
                {
                    ApplicationArea = All;
                    Caption = 'แคมเปญ';
                }
                field("SMK Car Body"; Rec."SMK Car Body")
                {
                    ApplicationArea = All;
                    Caption = 'Body';
                }
                field("SMK Car CC"; Rec."SMK Car CC")
                {
                    ApplicationArea = All;
                    Caption = 'ขนาด CC';
                }
                field("SMK Car Code"; Rec."SMK Car Code")
                {
                    ApplicationArea = All;
                    Caption = 'รหัสรถ';
                }
                field("SMK Car Group"; Rec."SMK Car Group")
                {
                    ApplicationArea = All;
                    Caption = 'กลุ่มรถ';
                }
                field("SMK Car Make"; Rec."SMK Car Make")
                {
                    ApplicationArea = All;
                    Caption = 'รุ่นรถ';
                }
                field("SMK Car Model"; Rec."SMK Car Model")
                {
                    ApplicationArea = All;
                    Caption = 'ปีรถ';
                }
                field("SMK Car Seat"; Rec."SMK Car Seat")
                {
                    ApplicationArea = All;
                    Caption = 'จำนวนที่นั่ง';
                }
                field("SMK Chasis"; Rec."SMK Chasis")
                {
                    ApplicationArea = All;
                    Caption = 'เลขคัสซี';
                }
                field("SMK Col. Send Receive Date"; Rec."SMK Col. Send Receive Date")
                {
                    ApplicationArea = All;
                    Caption = 'วันที่ฝ่ายจัดเก็บเบี้ยส่ง กธ. ให้ลูกค้า';
                }
                field("SMK Col. Receive Date"; Rec."SMK Col. Receive Date")
                {
                    ApplicationArea = All;
                    Caption = 'วันที่ฝ่ายจัดเก็บเบี้ยรับ กธ. จากฝ่ายผลิต';
                }
                field("SMK Col. Reconcile Date"; Rec."SMK Col. Reconcile Date")
                {
                    ApplicationArea = All;
                    Caption = 'วันที่ฝ่ายจัดเก็บเบี้ยตัดรับเบี้ย';
                }

                field("SMK Contract Date"; Rec."SMK Contract Date")
                {
                    ApplicationArea = All;
                    Caption = 'วันทำสัญญา';
                }
                field("SMK Customer Name"; Rec."SMK Customer Name")
                {
                    ApplicationArea = All;
                    Caption = 'ชื่อลูกค้า';
                }
                field("SMK Effect End Date"; Rec."SMK Effect End Date")
                {
                    ApplicationArea = All;
                    Caption = 'วันสิ้นสุดคุ้มครอง';
                }
                field("SMK End Date"; Rec."SMK End Date")
                {
                    ApplicationArea = All;
                    Caption = 'วันที่ทำสลักหลัง';
                }
                field("SMK End No."; Rec."SMK End No.")
                {
                    ApplicationArea = All;
                    Caption = 'เลขที่สลักหลัง';
                }
                field("SMK End Type"; Rec."SMK End Type")
                {
                    ApplicationArea = All;
                    Caption = 'ประเภทสลักหลัง';
                }
                field("SMK End Year"; Rec."SMK End Year")
                {
                    ApplicationArea = All;
                    Caption = 'ปีสลักหลัง';
                }
                field("SMK Engine"; Rec."SMK Engine")
                {
                    ApplicationArea = All;
                    Caption = 'เลขเครื่อง';
                }
                field("SMK License"; Rec."SMK License")
                {
                    ApplicationArea = All;
                    Caption = 'ทะเบียนรถ';
                }
                field("SMK Old Policy"; Rec."SMK Old Policy")
                {
                    ApplicationArea = All;
                    Caption = 'เลขกรรมธรรม์เดิม';
                }

                field("SMK Policy Type"; Rec."SMK Policy Type")
                {
                    ApplicationArea = All;
                    Caption = 'ประเภทกรรมธรรม์';
                }
                field("SMK Policy Year"; Rec."SMK Policy Year")
                {
                    ApplicationArea = All;
                    Caption = 'ปี กธ.';
                }
                field("SMK Premium Duty"; Rec."SMK Premium Duty")
                {
                    ApplicationArea = All;
                    Caption = 'อากร';
                }
                field("SMK Premium Net"; Rec."SMK Premium Net")
                {
                    ApplicationArea = All;
                    Caption = 'เบี้ยสุทธิ';
                }
                field("SMK Premium Summary"; Rec."SMK Premium Summary")
                {
                    ApplicationArea = All;
                    Caption = 'เบี้ยรวม';
                }
                field("SMK Premium Tax"; Rec."SMK Premium Tax")
                {
                    ApplicationArea = All;
                    Caption = 'ภาษี';
                }
                field("SMK Promotion"; Rec."SMK Promotion")
                {
                    ApplicationArea = All;
                    Caption = 'โปรโมชั่น';
                }
                field("SMK Protect Eff. End Date"; Rec."SMK Protect Eff. End Date")
                {
                    ApplicationArea = All;
                    Caption = 'วันสิ้นสุดคุ้มครอง';
                }
                field("SMK Protect Eff. Start Date"; Rec."SMK Protect Eff. Start Date")
                {
                    ApplicationArea = All;
                    Caption = 'วันเริ่มคุ้มครอง';
                }
                field("SMK Receipt Date"; Rec."SMK Receipt Date")
                {
                    ApplicationArea = All;
                    Caption = 'วันที่ใบเสร็จ';
                }
                field("SMK Receipt No."; Rec."SMK Receipt No.")
                {
                    ApplicationArea = All;
                    Caption = 'เลขที่ใบเสร็จ';
                }
                field("SMK Receipt Type"; Rec."SMK Receipt Type")
                {
                    ApplicationArea = All;
                    Caption = 'ประเภทใบเสร็จ';
                }
                field("SMK Running"; Rec."SMK Running")
                {
                    ApplicationArea = All;
                    Caption = 'ครั้งที่ทำสลักหลัง';
                }
                field("SMK Send Date"; Rec."SMK Send Date")
                {
                    ApplicationArea = All;
                    Caption = 'วันที่ฝ่ายผลิตส่ง กธ. ให้ฝ่ายจัดเก็บเบี้ย';
                }
                field("SMK Std. Commission %"; Rec."SMK Std. Commission %")
                {
                    ApplicationArea = All;
                    Caption = '% ค่าคอมฯ มาตรฐาน ตอนกำหนดเลข กธ.';
                }
                field("SMK Sticker No."; Rec."SMK Sticker No.")
                {
                    ApplicationArea = All;
                    Caption = 'เลขเครื่องหมาย';
                }
                field("SMK Sticker Year"; Rec."SMK Sticker Year")
                {
                    ApplicationArea = All;
                    Caption = 'ปีเลข Sticker';
                }
                field("SMK Weight"; Rec."SMK Weight")
                {
                    ApplicationArea = All;
                    Caption = 'น้ำหนัก';
                }
                field("SMK Year"; Rec."SMK Year")
                {
                    ApplicationArea = All;
                    Caption = 'ปีที่ประกัน';
                }
            }
        }
    }

}
