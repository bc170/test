page 60040 "SMK Front Activity Cues"
{
    Caption = 'Front Acticity Cues';
    PageType = CardPart;
    RefreshOnActivate = true;
    SourceTable = "SMK Cue";
    layout
    {
        area(Content)
        {
            cuegroup("SMK Front Receipt")
            {
                Caption = 'หน้าร้านใบรับ';
                field(SMKNOFrontRcptNotLock; Rec."SMK No. Front Rcpt. Not Lock")
                {
                    Caption = 'ใบรับเงินยังไม่ได้ Lock';
                    ApplicationArea = All;
                }
                field(SMKFrontRcptLock; Rec."SMK No. Front Rcpt. Lock")
                {
                    Caption = 'ใบรับเงินรอทำ RV';
                    ApplicationArea = All;
                }
                field(SMKFrontRcptRV; Rec."SMK No. Front Rcpt. RV")
                {
                    Caption = 'ใบรับเงินทำ RV แล้ว';
                    ApplicationArea = All;
                }
            }
        }
    }
}