page 60024 "SMK Agent Card"
{
    PageType = List;
    UsageCategory = Administration;
    SourceTable = "SMK Agent Card";
    caption = 'Agent Card';
    SourceTableTemporary = false;
    DeleteAllowed = true;
    ModifyAllowed = true;
    InsertAllowed = true;

    layout
    {
        area(Content)
        {
            repeater("Lines")
            {
                field("Customer No."; Rec."Customer No.")
                {
                    Visible = false;
                    Editable = false;
                }
                field("Card No."; Rec."Card No.")
                {
                    ApplicationArea = All;
                }
                field("Credit Amount"; Rec."Credit Amount")
                {
                    ApplicationArea = All;
                }
                field(Unlimit; Rec.Unlimit)
                {
                    ApplicationArea = All;
                }
                field("Used Credit"; Rec."Used Credit")
                {
                    caption = 'Used Credit Limit';
                    ApplicationArea = All;
                }
                field("Approve Status"; Rec."Approve Status")
                {
                    ApplicationArea = All;
                }
                field("Approve Date"; Rec."Approve Date")
                {
                    ApplicationArea = All;
                }
                field("Keyin User"; Rec."Keyin User")
                {
                    ApplicationArea = All;
                }

            }
        }
    }

    actions
    {
        area(Processing)
        {
            action(SendApprove)
            {
                ApplicationArea = all;
                Caption = 'Send Approve';
                Image = SendApprovalRequest;

                trigger OnAction()
                begin
                    llApprove := true;
                end;
            }
            action(CancelApprove)
            {
                ApplicationArea = all;
                Caption = 'Cancel Approve';
                Image = CancelApprovalRequest;

                trigger OnAction()
                begin
                    llApprove := false;
                end;
            }
        }
    }

    var
        llApprove: Boolean;
        CustNo: code[20];

    procedure SetCust(PCustNo: code[20])
    begin
        CustNo := PCustNo;
    end;
}