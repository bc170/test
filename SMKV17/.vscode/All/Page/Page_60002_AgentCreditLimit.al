page 60002 "SMK Agent Credit Limit"
{
    SourceTable = "SMK Credit Limit Entry";
    SourceTableTemporary = true;
    PageType = List;
    InsertAllowed = false;
    DeleteAllowed = false;
    layout
    {
        area(Content)
        {
            repeater("Content1")
            {
                field("SMK Level"; Rec."SMK Level")
                {
                    ApplicationArea = All;
                    Editable = false;
                }
                field("SMK Customer No."; Rec."SMK Customer No.")
                {
                    ApplicationArea = All;
                    Editable = false;
                }
                field("SMK Product No."; Rec."SMK Product Group No.")
                {
                    ApplicationArea = All;
                    Editable = false;
                }
                field("SMK Sub Product No."; Rec."SMK Product No.")
                {
                    ApplicationArea = All;
                    Editable = false;
                }
                field("SMK Credit Amount"; Rec."SMK Credit Amount")
                {
                    ApplicationArea = All;
                }
                field("SMK Temporary Amount"; Rec."SMK GetSummaryTempCreditLimit"(TODAY))
                {
                    ApplicationArea = All;
                }
                field("SMK Hold"; Rec."SMK Hold")
                {
                    ApplicationArea = All;
                }

            }
        }
    }

    actions
    {
        area(Processing)
        {
            action("SMK Temporary")
            {
                ApplicationArea = All;
                Caption = 'BW Temporary';
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                Image = AccountingPeriods;
                trigger OnAction()
                var
                    PageTemporary: Page "SMK Temp. Credit Limit";
                begin
                    Clear(PageTemporary);
                    PageTemporary."SMK SetFilter"(Rec."SMK Customer No.", Rec."SMK Product Group No.", Rec."SMK Product No.");
                    PageTemporary.RunModal();
                end;
            }
        }
    }

    trigger OnAfterGetRecord()
    begin
        Rec."SMK Credit Amount" := GetCreditLimit(Rec."SMK Customer No.", Rec."SMK Product Group No.", Rec."SMK Product No.");
    end;

    trigger OnModifyRecord(): Boolean
    var
        CreditLimit: Record "SMK Credit Limit Entry";
    begin
        CreditLimit.reset;
        CreditLimit.SetRange("SMK Customer No.", Rec."SMK Customer No.");
        CreditLimit.SetRange("SMK Product Group No.", Rec."SMK Product Group No.");
        CreditLimit.SetRange("SMK Product No.", Rec."SMK Product No.");
        if CreditLimit.FindFirst() then begin
            CreditLimit."SMK Credit Amount" := Rec."SMK Credit Amount";
            CreditLimit.Modify();
        end else begin
            CreditLimit.Reset();
            CreditLimit.Init();
            CreditLimit."SMK Customer No." := Rec."SMK Customer No.";
            CreditLimit."SMK Product Group No." := Rec."SMK Product Group No.";
            CreditLimit."SMK Product No." := Rec."SMK Product No.";
            CreditLimit."SMK Credit Amount" := Rec."SMK Credit Amount";
            CreditLimit.Insert();
        end;
    end;

    trigger OnOpenPage()
    var
        DimValueProdGroup: Record "Dimension Value";
        DimValueProd: Record "Dimension Value";
        CreditLimitTmp: Record "SMK Credit Limit Entry" temporary;
    begin
        Clear(CreditLimitTmp);
        Rec.reset;
        Rec.Init();
        Rec."SMK Level" := 1;
        Rec."SMK Customer No." := CustNo;
        Rec.Insert();
        DimValueProdGroup.reset;
        DimValueProdGroup.SetRange("Dimension Code", 'PRODUCTGROUP');
        DimValueProdGroup.SetFilter(Code, '<>%1', '');
        if DimValueProdGroup.FindSet() then begin
            repeat
                Rec.reset;
                Rec.Init();
                Rec."SMK Level" := 2;
                Rec."SMK Customer No." := CustNo;
                Rec."SMK Product Group No." := DimValueProdGroup.Code;
                Rec."SMK Product No." := '';
                Rec.Insert();
                DimValueProd.reset;
                DimValueProd.SetRange("Dimension Code", 'PRODUCT');
                DimValueProd.SetRange("SMK Ref. Prod. Group", DimValueProdGroup.Code);
                if DimValueProd.findset() then begin
                    repeat
                        Rec.reset;
                        Rec.Init();
                        Rec."SMK Level" := 3;
                        Rec."SMK Customer No." := CustNo;
                        Rec."SMK Product Group No." := DimValueProdGroup.Code;
                        Rec."SMK Product No." := DimValueProd.Code;
                        Rec.Insert();
                    until DimValueProd.Next() = 0;
                end;
            until DimValueProdGroup.Next() = 0;
        end;

    end;

    /// <summary> 
    /// Description for SetCust.
    /// </summary>
    /// <param name="PCustNo">Parameter of type code[20].</param>
    procedure SetCust(PCustNo: code[20])
    begin
        CustNo := PCustNo;
    end;

    /// <summary> 
    /// Description for GetCreditLimit.
    /// </summary>
    /// <param name="PCustNo">Parameter of type Code[20].</param>
    /// <param name="PProdGroup">Parameter of type Code[20].</param>
    /// <param name="PProd">Parameter of type Code[20].</param>
    /// <returns>Return variable "Decimal".</returns>
    procedure GetCreditLimit(PCustNo: Code[20]; PProdGroup: Code[20]; PProd: Code[20]): Decimal
    var
        CreditLimit: Record "SMK Credit Limit Entry";
    begin
        CreditLimit.reset;
        CreditLimit.SetRange("SMK Customer No.", PCustNo);
        CreditLimit.SetRange("SMK Product Group No.", PProdGroup);
        CreditLimit.SetRange("SMK Product No.", PProd);
        if CreditLimit.FindSet() then begin
            CreditLimit.CalcSums("SMK Credit Amount");
            exit(CreditLimit."SMK Credit Amount");
        end else
            exit(0);
    end;

    var
        CustNo: code[20];
}