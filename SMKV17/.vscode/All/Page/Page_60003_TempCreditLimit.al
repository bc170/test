page 60003 "SMK Temp. Credit Limit"
{
    SourceTable = "SMK Temp. Credit Limit Entry";
    PageType = List;
    InsertAllowed = false;
    DeleteAllowed = false;
    ModifyAllowed = false;
    layout
    {
        area(Content)
        {
            group(Keyin)
            {
                field(TempAmount; TempAmount)
                {
                    ApplicationArea = All;
                    Caption = 'Amount';
                }
                field(TempExpireDate; TempExpireDate)
                {
                    ApplicationArea = All;
                    Caption = 'Expire Date';
                }
            }
            group(Grouping)
            {
                Editable = false;
                field(FilterCustomer; FilterCustomer)
                {
                    ApplicationArea = All;
                    Editable = false;
                }
                field(FilterProductGroup; FilterProductGroup)
                {
                    ApplicationArea = All;
                    Editable = false;
                }
                field(FilterProduct; FilterProduct)
                {
                    ApplicationArea = All;
                    Editable = false;
                }
            }
            repeater("Content1")
            {
                field("SMK Credit Amount"; Rec."SMK Credit Amount")
                {
                    ApplicationArea = All;
                    Editable = false;
                }
                field("SMK Expire Date"; Rec."SMK Expire Date")
                {
                    ApplicationArea = All;
                    Editable = false;
                }
                field("SMK Keyin Date"; Rec."SMK Keyin Date")
                {
                    ApplicationArea = All;
                    Editable = false;
                }
                field("SMK Keyin User"; Rec."SMK Keyin User")
                {
                    ApplicationArea = All;
                    Editable = false;
                }
                field("SMK Cancelled"; Rec."SMK Cancelled")
                {
                    ApplicationArea = All;
                    Editable = false;
                }
                field("SMK Cancelled Date"; Rec."SMK Cancelled Date")
                {
                    ApplicationArea = All;
                    Editable = false;
                }
                field("SMK Cancelled User"; Rec."SMK Cancelled User")
                {
                    ApplicationArea = All;
                    Editable = false;
                }
            }
        }
    }

    actions
    {
        area(Processing)
        {
            action("Add")
            {
                ApplicationArea = All;
                Caption = 'Add';
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                Image = Add;
                trigger OnAction()
                var
                    TempCreditLimit: Record "SMK Temp. Credit Limit Entry";
                begin

                    if TempAmount = 0 then
                        Error('Amount must have a value !');
                    if TempExpireDate = 0D then
                        Error('Expire Date must have a value !');

                    if not Confirm(StrSubstNo('Do you want to create Temporary Credit Limit for\Customer No. = %1\Product Group = %2\Sub Product Group = %3\Amount = %4\Expire Date = %5 ?'
                        , FilterCustomer
                        , FilterProductGroup
                        , FilterProduct
                        , TempAmount
                        , TempExpireDate)) then
                        exit;

                    TempCreditLimit.reset;
                    TempCreditLimit.Init();
                    TempCreditLimit."SMK Customer No." := FilterCustomer;
                    TempCreditLimit."SMK Product Group No." := FilterProductGroup;
                    TempCreditLimit."SMK Product No." := FilterProduct;
                    TempCreditLimit.Validate("SMK Credit Amount", TempAmount);
                    TempCreditLimit.Validate("SMK Expire Date", TempExpireDate);
                    TempCreditLimit.Insert(true);
                    TempAmount := 0;
                    TempExpireDate := 0D;
                end;
            }
            action("Cancel")
            {
                ApplicationArea = All;
                Caption = 'Cancel';
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                Image = Add;
                trigger OnAction()
                begin
                    if not Confirm('Do you want to cancel this Temporary Credit Limit ?') then
                        exit;
                    Rec.Validate("SMK Cancelled", true);
                    Rec.Modify();
                end;
            }
        }
    }

    trigger OnOpenPage()
    begin
        Rec.FilterGroup(0);
        Rec.SetRange("SMK Customer No.", FilterCustomer);
        Rec.SetRange("SMK Product Group No.", FilterProductGroup);
        Rec.SetRange("SMK Product No.", FilterProduct);
        Rec.FilterGroup(2);
    end;

    /// <summary> 
    /// Description for SMK SetFilter.
    /// </summary>
    /// <param name="PFilterCust">Parameter of type code[20].</param>
    /// <param name="PFilterProdGroup">Parameter of type code[20].</param>
    /// <param name="PFilterProd">Parameter of type Code[20].</param>
    procedure "SMK SetFilter"(PFilterCust: code[20]; PFilterProdGroup: code[20]; PFilterProd: Code[20])
    begin
        FilterCustomer := PFilterCust;
        FilterProductGroup := PFilterProdGroup;
        FilterProduct := PFilterProd;
    end;

    var
        FilterCustomer: code[20];
        FilterProductGroup: code[20];
        FilterProduct: code[20];
        TempAmount: Decimal;
        TempExpireDate: Date;
}